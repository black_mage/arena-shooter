﻿using UnityEngine;

namespace Assets.Scripts.Abstract.Effects
{
	public abstract class PlayableEffect : MonoBehaviour
	{
		public abstract void Play();
	}
}
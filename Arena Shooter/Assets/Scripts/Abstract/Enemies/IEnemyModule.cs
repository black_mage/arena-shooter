﻿using UnityEngine;

namespace Assets.Scripts.Abstract.Enemies
{
	public interface IEnemyModule
	{
		Vector3 Process(Vector3 target);
	}
}
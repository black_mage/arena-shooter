﻿using System;
using Assets.Scripts.MonoBehaviours.Enemies;

namespace Assets.Scripts.Abstract.Enemies
{
	public interface IEnemySpawn
	{
		void Update();
		void Destroy();
		event Action<IEnemySpawn,BasicEnemy> OnSpawned;
	}
}
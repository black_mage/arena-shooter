﻿using Assets.Scripts.MonoBehaviours.General.Movables;

namespace Assets.Scripts.Abstract.General
{
	public interface IMovableProvider
	{
		Movable Movable { get; }
	}
}
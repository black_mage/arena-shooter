﻿using UnityEngine;

namespace Assets.Scripts.Abstract.General
{
	public abstract class VelocityProvider : MonoBehaviour
	{
		public abstract Vector3 Velocity { get; }
	}
}
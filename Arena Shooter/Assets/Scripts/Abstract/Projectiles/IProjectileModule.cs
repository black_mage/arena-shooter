﻿using UnityEngine;

namespace Assets.Scripts.Abstract.Projectiles
{
	public interface IProjectileModule
	{
		Vector3 Process();
	}
}
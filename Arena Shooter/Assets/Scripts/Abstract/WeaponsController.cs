﻿using Assets.Scripts.MonoBehaviours.Player;
using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.Abstract
{
	public abstract class WeaponsController
	{
		protected readonly PlayerBehaviour PlayerBehaviour;

		protected BasicWeapon _weapon;

		


		protected WeaponsController(BasicWeapon weapon)
		{
			
			PlayerBehaviour = Object.FindObjectOfType<PlayerBehaviour>();
			_weapon = weapon;
			

			ChangeWeapon(_weapon);

			

		}

		public BasicWeapon Weapon
		{
			get { return _weapon; }
		}


		public void ChangeWeapon(BasicWeapon weapon)
		{
			_weapon = (BasicWeapon)Object.Instantiate(weapon, PlayerBehaviour.transform.position, Quaternion.identity);
			_weapon.transform.parent = PlayerBehaviour.transform;

		}

		protected void FireWeapon(float angle)
		{
			_weapon.Fire(angle,PlayerBehaviour.GetActiveMutators());

			
		}

		public abstract void HandleWeapons();
	}
}
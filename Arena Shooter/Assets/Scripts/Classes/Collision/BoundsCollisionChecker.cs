﻿using Assets.Scripts.Data.Collision;

namespace Assets.Scripts.Classes.Collision
{
	public static class BoundsCollisionChecker
	{
		
		public static CollisionResponse CheckCollision(CollisionCheckRequest request)
		{
			var bounds = request.RectBounds;
			
			if (request.NewPosition.x-request.Radius < bounds.Left || request.NewPosition.x+request.Radius > bounds.Right)
				return new CollisionResponse(true, CollisionLineOrientation.Vertical);

			if (request.NewPosition.y-request.Radius < bounds.Bottom || request.NewPosition.y+request.Radius >bounds.Top)
				return new CollisionResponse(true, CollisionLineOrientation.Horizontal);

			return new CollisionResponse(false, CollisionLineOrientation.None);
		}




	}


}
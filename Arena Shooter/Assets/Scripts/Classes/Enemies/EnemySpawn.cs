﻿using System;
using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.Enemies;
using Assets.Scripts.MonoBehaviours.Player;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Classes.Enemies
{
	public class EnemySpawn : IEnemySpawn
	{
		private BasicEnemy _enemyType;
		private Vector3 _spawnPosition;
		private LaserEffect _laserEffect;
		private float _secondsUntilSpawn;
		private float _elapsedTime;
		

		public event Action<IEnemySpawn,BasicEnemy> OnSpawned; 

		public EnemySpawn(BasicEnemy enemyType, LaserEffect laserEffect, float secondsUntilSpawn, Vector3 spawnPosition)
		{
			_enemyType = enemyType;
			_laserEffect = laserEffect;
			_secondsUntilSpawn = secondsUntilSpawn;
			_spawnPosition = spawnPosition;
			

			_laserEffect = (LaserEffect)Object.Instantiate(_laserEffect);
			_laserEffect.transform.position = spawnPosition;

		
		}



		public void Update()
		{
			_elapsedTime += Time.deltaTime;

			_laserEffect.Interpolate(_elapsedTime/_secondsUntilSpawn);
			if(_elapsedTime >= _secondsUntilSpawn)
			{

				var enemy = (BasicEnemy) Object.Instantiate(_enemyType, _spawnPosition, Quaternion.identity);
		
				if (OnSpawned != null)
					OnSpawned(this, enemy);
				
				Object.Destroy(_laserEffect.gameObject);

				
			}
		}

		public void Destroy()
		{
			Object.Destroy(_laserEffect.gameObject);
		}
	}
}
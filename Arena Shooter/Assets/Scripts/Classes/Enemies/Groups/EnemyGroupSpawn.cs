﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.Enemies;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Classes.Enemies.Groups
{
	public class EnemyGroupSpawn : IEnemySpawn
	{
		public event Action<IEnemySpawn, BasicEnemy> OnSpawned;

		private readonly BasicEnemy _enemyType;
		private float _secondsUntilSpawn;
		private float _elapsedTime;
		private InGameStateManager _gameStateManager;
		private readonly IEnumerable<Vector3> _spawnPositions;

		private readonly LaserBoxEffect _laserEffect;

		

		public EnemyGroupSpawn(BasicEnemy enemyType, float secondsUntilSpawn,LaserBoxEffect laserEffect,IEnumerable<Vector3> spawnPositions )
		{
			_enemyType = enemyType;
			_secondsUntilSpawn = secondsUntilSpawn;
			_spawnPositions = spawnPositions;

			_laserEffect = laserEffect;

			_laserEffect = (LaserBoxEffect)Object.Instantiate(_laserEffect,spawnPositions.First(),Quaternion.identity);
			
			_laserEffect.CreatePath(spawnPositions,enemyType.BoundsCollisionRadius);

			_gameStateManager = GameObject.FindObjectOfType<InGameStateManager>();

		}

		public void Update()
		{
			if (_gameStateManager.CurrentState != GameState.Playing)
				return;

			_elapsedTime += Time.deltaTime;

			_laserEffect.Interpolate(_elapsedTime / _secondsUntilSpawn);
			if(_elapsedTime >= _secondsUntilSpawn )
			{
				
				foreach (var spawnPosition in _spawnPositions)
				{
					
					var enemy = (BasicEnemy) Object.Instantiate(_enemyType, spawnPosition, Quaternion.identity);
					
				
					if (OnSpawned != null)
						OnSpawned(this, enemy);

					Object.Destroy(_laserEffect.gameObject);
				}
				
			}
		}

		public void Destroy()
		{
			Object.Destroy(_laserEffect.gameObject);
		}
	}
}
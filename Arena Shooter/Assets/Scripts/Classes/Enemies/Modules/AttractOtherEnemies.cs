﻿﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abstract.Enemies;
﻿using Assets.Scripts.Config.Enemies;
﻿using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class AttractOtherEnemies : IEnemyModule
	{
		private BasicEnemy _owner;
		private readonly AttractOtherEnemiesConfiguration _config;

		public AttractOtherEnemies(BasicEnemy owner,AttractOtherEnemiesConfiguration config)
		{
			_owner = owner;
			_config = config;
		}

		public Vector3 Process(Vector3 target)
		{
			var nearestEnemy = _owner.EnemyDistancesSquared.Where(kv =>kv.Key.CanBeAttractedByOthers==true && kv.Value <= (_config.AttractRadius * _config.AttractRadius)).OrderBy(dist => dist.Value);


			foreach (var enemy in nearestEnemy.Select(e => e.Key))
			{
				var direction = new Vector3(_owner.transform.position.x - enemy.transform.position.x, 0,
				                            _owner.transform.position.z - enemy.transform.position.z).normalized;





				if (_owner.ModuleConfigurations.AttractOtherEnemiesConfig.AttractionIdentity ==
				    enemy.ModuleConfigurations.AttractOtherEnemiesConfig.AttractionIdentity)
					return Vector3.zero;


				enemy.AddVelocity(direction*enemy.BaseSpeed*_owner.ModuleConfigurations.AttractOtherEnemiesConfig.AttractMultiplier*
				                  _config.Influence);

			}
			return Vector3.zero;
		}
	}
}


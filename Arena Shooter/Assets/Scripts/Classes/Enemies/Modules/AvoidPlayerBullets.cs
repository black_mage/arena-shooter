﻿using System;
using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class AvoidPlayerBullets : IEnemyModule
	{
		private readonly BasicEnemy _owner;
		private float _difficulty;


		public AvoidPlayerBullets(BasicEnemy owner, float difficulty)
		{
			_owner = owner;
			_difficulty = difficulty;
		}

		public Vector3 Process(Vector3 target)
		{
			if (_owner.TargetPlayer.CurrentWeapon.CanFire == true)
				return Vector3.zero;
			var angleRange = _owner.TargetPlayer.CurrentWeapon.GetCurrentFiringAngles();
			
			

			if (angleRange == Vector2.zero)
				return Vector3.zero;

			
			
			


			var playerPos = _owner.TargetPlayer.transform.position;
			var pos = _owner.transform.position;

			var angle = Mathf.Atan2(pos.x - playerPos.x, pos.z - playerPos.z)*Mathf.Rad2Deg;

			
			

			var a = angleRange.x%360;
			var b = angleRange.y%360;
			var c = (angle)%360;

			if (a < 0) a += 360f;
			if (b < 0) b += 360f;
			if (c < 0) c += 360f;
				

			
			
			
			

			var within=false;



			
			var ca = Mathf.DeltaAngle(c, a);
			var cb = Mathf.DeltaAngle(c, b);


			

			if (ca < 0f && cb > 0f)
				within = true;


			if (within)
			{
				

				var targetDirection = Mathf.Min(Mathf.Abs(ca), Mathf.Abs(cb));

				var playerAngle = 0f;
				if(targetDirection == Mathf.Abs(ca))
				{
					playerAngle = a+-1*_difficulty;

				}
				else
				{
					playerAngle = b+_difficulty;
				}

				
				
				var distance = Vector3.Distance(playerPos, pos);

				var targetPosition = (new Vector3(Mathf.Sin(playerAngle*Mathf.Deg2Rad), 0,
				                                 Mathf.Cos(playerAngle*Mathf.Deg2Rad))*distance)+playerPos;

				

				var targetAngle = Mathf.Atan2(targetPosition.x - pos.x, targetPosition.z - pos.z);

				
				
				return new Vector3(Mathf.Sin(targetAngle),0,Mathf.Cos(targetAngle))*(_owner.BaseSpeed*Time.deltaTime);
			}

			return Vector3.zero;
		}
	}
}
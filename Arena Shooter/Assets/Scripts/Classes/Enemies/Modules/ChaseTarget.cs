﻿using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.Config.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class ChaseTarget : IEnemyModule
	{
		private readonly BasicEnemy _owner;
		private readonly ChaseTargetConfig _config;


		private float _remainingBurstTime;

		
		
		public ChaseTarget(BasicEnemy owner,ChaseTargetConfig config)
		{
			_owner = owner;
			_config = config;
		}

		public Vector3 Process(Vector3 target)
		{
	
			var workingPos = target - _owner.transform.position;
			var targetAngle = Mathf.Atan2(workingPos.x, workingPos.z) * Mathf.Rad2Deg;
			var currentAngle = _owner.transform.rotation.eulerAngles.y;


			var shortestDistance = Mathf.DeltaAngle(currentAngle, targetAngle);
			if (shortestDistance < 0)
			{
				_owner.transform.Rotate(0, -1*(_owner.TurnSpeed*Time.deltaTime), 0);
			}
			else
			{
					
				_owner.transform.Rotate(0, _owner.TurnSpeed*Time.deltaTime, 0);
			}

			return
					new Vector3(Mathf.Sin(_owner.transform.eulerAngles.y * Mathf.Deg2Rad), 0,
								Mathf.Cos(_owner.transform.eulerAngles.y * Mathf.Deg2Rad)) * (_owner.BaseSpeed * Time.deltaTime);
			

	
		}
	}
}
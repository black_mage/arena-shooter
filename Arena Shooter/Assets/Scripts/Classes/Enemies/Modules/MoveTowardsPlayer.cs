﻿using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class MoveTowardsPlayer : IEnemyModule
	{
		private BasicEnemy _owner;

		public MoveTowardsPlayer(BasicEnemy owner)
		{
			_owner = owner;
		}

		public Vector3 Process(Vector3 target)
		{
			var workingPos = _owner.TargetPlayer.transform.position - _owner.transform.position;
			var angle = Mathf.Atan2(workingPos.x, workingPos.z);
			return new Vector3(Mathf.Sin(angle),0,Mathf.Cos(angle))*(_owner.BaseSpeed*Time.deltaTime);
		}
	}
}
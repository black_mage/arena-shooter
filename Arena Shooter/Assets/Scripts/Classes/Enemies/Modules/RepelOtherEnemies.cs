﻿﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abstract.Enemies;
﻿using Assets.Scripts.Config.Enemies;
﻿using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class RepelOtherEnemies : IEnemyModule
	{
		private BasicEnemy _owner;
		private readonly RepelOtherEnemiesConfiguration _config;

		public RepelOtherEnemies(BasicEnemy owner,RepelOtherEnemiesConfiguration config)
		{
			_owner = owner;
			_config = config;
		}

		public Vector3 Process(Vector3 target)
		{


			var nearestEnemy = _owner.EnemyDistancesSquared.Where(kv => kv.Value <= (_config.RepelRadius * _config.RepelRadius) ).OrderBy(dist => dist.Value).FirstOrDefault();

			

			if (nearestEnemy.Equals(default(KeyValuePair<BasicEnemy,float>)) )
				return Vector3.zero;

			
			var enemy = nearestEnemy.Key;

			if ((enemy.EnemyTypeName == _owner.EnemyTypeName && _owner.CanBeRepelledBySame == false) || (enemy.EnemyTypeName != _owner.EnemyTypeName && _owner.CanBeRepelledByDifferent == false))
				return Vector3.zero;
			
			var direction = new Vector3(_owner.transform.position.x - enemy.transform.position.x, 0,
			                            _owner.transform.position.z - enemy.transform.position.z).normalized;


			var multiplier = enemy.ModuleConfigurations.RepelOtherEnemiesConfig.Enabled && enemy.ModuleConfigurations.RepelOtherEnemiesConfig.RepulsionIdentity != _config.RepulsionIdentity
				                 ? enemy.ModuleConfigurations.RepelOtherEnemiesConfig.RepelMultiplier
				                 : 1f;


			return direction*_owner.BaseSpeed*multiplier;
		}
	}
}


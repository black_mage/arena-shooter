﻿using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class RoamLevel : IEnemyModule
	{
		private LevelManager _levelManager;
		private BasicEnemy _owner;
		private Vector3 _destination;

		public RoamLevel(BasicEnemy owner)
		{
			_owner = owner;
			_levelManager = GameObject.FindObjectOfType<LevelManager>();

			PickDestination();

		}

		private void PickDestination()
		{
			var bounds = _levelManager.LevelInfo.RectBounds;

			var x = Random.Range(bounds.Left + _owner.BoundsCollisionRadius*2, bounds.Right - _owner.BoundsCollisionRadius*2);
			var z = Random.Range(bounds.Bottom + _owner.BoundsCollisionRadius*2, bounds.Top - _owner.BoundsCollisionRadius*2);
			_destination = new Vector3(x, 0, z);
		}

		public Vector3 Process(Vector3 target)
		{
			if(Vector3.SqrMagnitude(_destination-_owner.transform.position) < 100)
				PickDestination();

			var workingPos = _destination - _owner.transform.position;
			var targetAngle = Mathf.Atan2(workingPos.x, workingPos.z) * Mathf.Rad2Deg;
			var currentAngle = _owner.transform.rotation.eulerAngles.y;

			var shortestDistance = Mathf.DeltaAngle(currentAngle, targetAngle);



			if (shortestDistance < 0)
			{
				_owner.transform.Rotate(0, -1 * (_owner.TurnSpeed * Time.deltaTime), 0);
			}
			else
			{

				_owner.transform.Rotate(0, _owner.TurnSpeed * Time.deltaTime, 0);
			}

			return
					new Vector3(Mathf.Sin(_owner.transform.eulerAngles.y * Mathf.Deg2Rad), 0,
								Mathf.Cos(_owner.transform.eulerAngles.y * Mathf.Deg2Rad)) * (_owner.BaseSpeed * Time.deltaTime);
			

		}
	}
}
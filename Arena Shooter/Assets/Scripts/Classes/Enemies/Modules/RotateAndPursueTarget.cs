﻿using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.Classes.Enemies.Modules
{
	public class RotateAndPursueTarget : IEnemyModule
	{
		private BasicEnemy _owner;
		

		private bool _turning;
		private float _remainingBurstTime;

		public float BurstLength = 3f;
		
		public RotateAndPursueTarget(BasicEnemy owner,float burstLength)
		{
			_owner = owner;
			
			_turning = true;

			BurstLength = burstLength;
		}

		public Vector3 Process(Vector3 target)
		{
			if (_turning)
			{
				var workingPos = target - _owner.transform.position;
				var targetAngle = Mathf.Atan2(workingPos.x, workingPos.z) * Mathf.Rad2Deg;
				var currentAngle = _owner.transform.rotation.eulerAngles.y;

				var shortestDistance = Mathf.DeltaAngle(currentAngle, targetAngle);
				if (Mathf.Abs(shortestDistance) < _owner.TurnSpeed*Time.deltaTime)
				{
					_turning = false;
					_remainingBurstTime = BurstLength;
					return Vector3.zero;
				}
				

				if (shortestDistance < 0)
				{
					
					_owner.transform.Rotate(0, -1*(_owner.TurnSpeed*Time.deltaTime), 0);
				}
				else
				{
					
					_owner.transform.Rotate(0, _owner.TurnSpeed*Time.deltaTime, 0);
				}
				
			}
			else
			{
				if (_remainingBurstTime > 0f)
				{

					_remainingBurstTime -= Time.deltaTime;
					return
						new Vector3(Mathf.Sin(_owner.transform.eulerAngles.y*Mathf.Deg2Rad), 0,
						            Mathf.Cos(_owner.transform.eulerAngles.y*Mathf.Deg2Rad))*(_owner.BaseSpeed*Time.deltaTime);
				}
				else
				{
					_turning = true;
					
				}
			}
			return Vector3.zero;
		}
	}
}
﻿using System.Linq;
using Assets.Scripts.Config.Level;
using UnityEngine;

namespace Assets.Scripts.Classes.Level
{
	public class LevelBuilder
	{

		


		public LevelInfo BuildLevel(LevelConfig config)
		{
			var mapSizeX = config.Dimensions.x;
			var mapSizeY = config.Dimensions.y;

			var levelGo = new GameObject("Level");

			var xPos = (mapSizeX/config.Density.x)/2;
			var zPos = (mapSizeY/config.Density.y)/2;
			for(var x=0; x<config.Density.y;x++)
			{
				
				for(var z=0; z<config.Density.x;z++)
				{
					var go = GameObject.CreatePrimitive(PrimitiveType.Plane);

					
					
					foreach (var materialEntry in config.FloorMaterials)
					{
						materialEntry.Material.mainTextureScale = new Vector2((1 / materialEntry.Tiling.x) * mapSizeX, (1 / materialEntry.Tiling.y) * mapSizeY);
					}

					go.GetComponent<Renderer>().materials = config.FloorMaterials.Select(s => s.Material).ToArray();

					go.transform.parent = levelGo.transform;
					go.transform.localScale = new Vector3(mapSizeX / 10/config.Density.x, 1f, mapSizeY / 10/config.Density.y);
					go.transform.position = new Vector3(xPos, 0f, zPos);

					xPos += mapSizeX / config.Density.x;
				}
				xPos = (mapSizeX / config.Density.x) / 2;
				zPos += mapSizeY/config.Density.y;
			}

			

			

			return new LevelInfo(levelGo,mapSizeX,mapSizeY);
		}
	}
}
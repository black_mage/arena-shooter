﻿using System;
using Assets.Scripts.Data.Collision;
using Assets.Scripts.Data.Level;
using UnityEngine;

namespace Assets.Scripts.Classes.Level
{
	public class LevelInfo
	{
		

		
		public float TotalWidth { get; private set; }
		public float TotalHeight { get; private set; }
		public GameObject LevelObject;

		private Vector3 _lastGOPosition;
		private RectBounds _currentRectBounds;
		public RectBounds RectBounds 
		{
			get
			{
				return _currentRectBounds;
			}
		
		
		}

		public LevelInfo(GameObject levelObject,float totalWidth, float totalHeight)
		{
			
			TotalWidth = totalWidth;
			TotalHeight = totalHeight;
			LevelObject = levelObject;

			_currentRectBounds = new RectBounds(levelObject.transform.position.x,levelObject.transform.position.z,TotalWidth,TotalHeight);
		}



		
	}
}
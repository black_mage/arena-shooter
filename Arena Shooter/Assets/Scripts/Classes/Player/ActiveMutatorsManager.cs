﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Classes.Projectile.Mutators;
using Assets.Scripts.Data.Pickups;
using UnityEngine;

namespace Assets.Scripts.Classes.Player
{
	public class ActiveMutatorsManager
	{
		private List<WeaponMutatorEntry> _mutators;

		public ActiveMutatorsManager()
		{
			_mutators = new List<WeaponMutatorEntry>();
		}

		public void Update()
		{
			foreach (var entry in _mutators)
				entry.TakeLife(Time.deltaTime);

			_mutators.RemoveAll(e => e.Life <= 0f);
		}

		public void AddMutator(WeaponMutator mutator, float duration)
		{
			_mutators.Add(new WeaponMutatorEntry(mutator, duration));
		}

		public IEnumerable<WeaponMutator> GetActiveMutators()
		{
			return _mutators.Select(m => m.Mutator);
		}
	}
}
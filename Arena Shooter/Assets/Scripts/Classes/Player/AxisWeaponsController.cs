﻿using Assets.Scripts.Abstract;
using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.Classes.Player
{
	public class AxisWeaponsController : WeaponsController
	{
		public AxisWeaponsController(BasicWeapon weapon) : base(weapon)
		{
		}

		public override void HandleWeapons()
		{

			var axes = new Vector3(Input.GetAxis("FireHorizontal"), 0, Input.GetAxis("FireVertical"));

			if (axes.magnitude == 0f)
				return;

			var direction =axes.normalized;

			
			
			var angle = Mathf.Atan2(direction.x,direction.z)*Mathf.Rad2Deg;
			
			FireWeapon(angle);
			
				
		}
	}
}
﻿using Assets.Scripts.Abstract.General;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.Classes.Player
{
	public class PlayerController
	{
		
		public IMovableProvider MovableProvider;

		private readonly LevelManager _levelManager;


		

		
		
		public PlayerController(IMovableProvider movableProvider)
		{
			MovableProvider = movableProvider;
			_levelManager = GameObject.FindObjectOfType<LevelManager>();
			
			
		}

		public void Start()
		{
			MovableProvider.Movable.SetBounds(_levelManager.LevelInfo.RectBounds);
		}

		public void Update()
		{
			var movement = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical")).normalized;
			MovableProvider.Movable.Move(movement);

			



			
		}
	}
}
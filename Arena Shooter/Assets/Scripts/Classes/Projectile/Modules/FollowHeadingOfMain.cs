﻿using Assets.Scripts.Abstract.Projectiles;
using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;

namespace Assets.Scripts.Classes.Projectile.Modules
{
	public class FollowHeadingOfMain : IProjectileModule
	{
		private readonly BasicProjectile _owner;

		private readonly float _timeToTake;
		private float _timeRemaining;

		private bool _active;
		private bool _stopWhenHeadingReached;
		private Vector3 _progress;

		public FollowHeadingOfMain(BasicProjectile owner,FollowHeadingOfMainConfig config)
		{
			_active = true;
			_stopWhenHeadingReached = config.StopWhenMainHeadingReached;
			_owner = owner;
			_timeToTake = config.TimeToTake;
			_timeRemaining = _timeToTake;

			var currentHeading = _owner.Heading;
			var destHeading = _owner.HeadingOfMainProjectile;

			if (currentHeading == destHeading)
			{
				_active = false;
			}
		}

		public bool Active
		{
			get { return _active; }
			
		}

		public Vector3 Process()
		{
			if (!_active)
				return _progress;

			var currentHeading = _owner.Heading;
			var destHeading = _owner.HeadingOfMainProjectile;

			_timeRemaining -= Time.deltaTime;

			var percent = Mathf.Clamp(1-(_timeRemaining/_timeToTake),0f,1f);

			

			if (percent >= 1f)
				_active = false;


			_progress = Vector3.Lerp(currentHeading, destHeading, percent);
			return _progress;


		}
	}
}
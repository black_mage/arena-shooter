﻿using System;
using Assets.Scripts.Abstract.Projectiles;
using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.MonoBehaviours.Enemies;
using Assets.Scripts.MonoBehaviours.Managers;
using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Classes.Projectile.Modules
{
	public class HomeOnEnemy : IProjectileModule
	{
		private BasicProjectile _owner;
		private BasicEnemy _target;
		private EnemyManager _enemyManager;

		private bool _followClosestEnemy;

		public HomeOnEnemy(BasicProjectile owner,HomeOnEnemyConfiguration config)
		{
			_followClosestEnemy = config.FollowClosestEnemy;
			_owner = owner;

			_enemyManager = Object.FindObjectOfType<EnemyManager>();
			

			var enemies = _enemyManager.Enemies;

			var lastDist = Mathf.Infinity;
			_target = null;
			foreach (var enemy in enemies)
			{
				var distance = Vector3.SqrMagnitude(enemy.transform.position - _owner.transform.position);
				lastDist = Mathf.Min(lastDist, distance);

				if (Math.Abs(distance - lastDist) < Mathf.Epsilon)
					_target = enemy;
			}
		}


		public Vector3 Process()
		{
			if(_followClosestEnemy)
			{
				var enemies = _enemyManager.Enemies;
				var lastDist = Mathf.Infinity;
				_target = null;
				foreach (var enemy in enemies)
				{
					var distance = Vector3.SqrMagnitude(enemy.transform.position - _owner.transform.position);
					lastDist = Mathf.Min(lastDist, distance);

					if (Math.Abs(distance - lastDist) < Mathf.Epsilon)
						_target = enemy;
				}
			}
			if (_target != null)
			{
				var workingPos = _target.transform.position - _owner.transform.position;
				var targetAngle = Mathf.Atan2(workingPos.x, workingPos.z) * Mathf.Rad2Deg;
				var currentAngle = _owner.transform.rotation.eulerAngles.y;

				var shortestDistance = Mathf.DeltaAngle(currentAngle, targetAngle);

				if (shortestDistance < 0)
				{
					_owner.transform.Rotate(0, -1 * _owner.Config.TurnSpeed * Time.deltaTime, 0);
				}
				else
				{
					_owner.transform.Rotate(0, _owner.Config.TurnSpeed * Time.deltaTime, 0);
				}

				return new Vector3(Mathf.Sin(_owner.transform.eulerAngles.y * Mathf.Deg2Rad), 0,
									Mathf.Cos(_owner.transform.eulerAngles.y * Mathf.Deg2Rad));
			}

			return Vector3.zero;
		}
	}
}
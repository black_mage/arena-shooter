﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Abstract.Projectiles;
using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;

namespace Assets.Scripts.Classes.Projectile.Modules
{
	public class ZigZag : IProjectileModule
	{
		private readonly BasicProjectile _owner;

		

		private List<float> _destAngles;
		private int _currentDest;

		private float _totalTime;
		private float _timePerAngle;
		private float _timeRemaining;

		private Vector3 _startDirection;
		private Vector3 _destDirection;
		private bool _started;

		public ZigZag(BasicProjectile owner,ZigZagConfiguration config)
		{

			_owner = owner;
			
			

			

			_destAngles = new List<float>();

			var burstCoefficient = owner.BurstIndex > 0 ? -1 : 1;
			var angle = config.Angle;
			angle *= burstCoefficient;
			
			_destAngles.Add(angle);
			_destAngles.Add(-1*angle);
			_destAngles.Add(-1*angle);
			_destAngles.Add(angle);

			_currentDest = 0;

			_totalTime = config.TimeToTake;
			_timePerAngle = _totalTime/_destAngles.Count;
			_timeRemaining = _timePerAngle;

			ComputeDirections();

		}

		public Vector3 Process()
		{

			if (_owner.IsAligningWithMainProjectile || _owner.BurstIndex == 0)
			{
				return Vector3.zero;
			}

			if(!_owner.IsAligningWithMainProjectile && !_started)
			{
				_started = true;
				_timeRemaining = _timePerAngle;
			}

			_timeRemaining -= Time.deltaTime;
			var percent = 1f - (_timeRemaining/_timePerAngle);

			var retVal = Vector3.Lerp(_startDirection, _destDirection, percent)-_startDirection;

			
			if(percent >= 1f)
			{
				
				_currentDest++;
				_timeRemaining = _timePerAngle;
				if(_currentDest == _destAngles.Count)
					_currentDest = 0;

				
				ComputeDirections();
			}

			return retVal;
			
		}

		private void ComputeDirections()
		{
			_startDirection = _owner.Velocity.normalized;
			var startAngle = Mathf.Atan2(_startDirection.x, _startDirection.z) * Mathf.Rad2Deg;
			var destAngle = _destAngles[_currentDest] + startAngle % 360;

			_destDirection = new Vector3(Mathf.Sin(destAngle*Mathf.Deg2Rad), 0, Mathf.Cos(destAngle*Mathf.Deg2Rad));
		}
	}
}
﻿using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.MonoBehaviours.Projectiles;

namespace Assets.Scripts.Classes.Projectile.Mutators
{
	public class WeaponMutator
	{
		private readonly WeaponMutatorConfiguration _config;

		public WeaponMutator(WeaponMutatorConfiguration config)
		{
			_config = config;
		}


		public void Mutate(BasicProjectile projectile)
		{
			projectile.Config.Damage = (int)((float)projectile.Config.Damage * _config.DamageMultiplier);
			projectile.AddModulesFromConfig(_config.Modules);
			projectile.MutateConfig(_config.Config);
		}

	}
}
﻿using System;
using UnityEngine;

namespace Assets.Scripts.Classes.Scoring
{
	[Serializable]
	public class PointsSchedule
	{
		public int BaseScore;
		public int Bonus;
		public float BonusDuration;
		private float _remainingDuration;
		public AnimationCurve BonusFalloff;


		public void Start()
		{
			_remainingDuration = BonusDuration;
		}

		public void Update(float deltaTime)
		{
			_remainingDuration -= deltaTime;
		}

		public int GetBonusValue()
		{
			
			var coefficient = 1 - (_remainingDuration/BonusDuration);

			return (int)(BonusFalloff.Evaluate(coefficient)*Bonus);
		}
	}
}
﻿using Assets.Scripts.MonoBehaviours.Weapons;

namespace Assets.Scripts.Classes.Weapons
{
	public class WeaponsManager
	{
		private BasicWeapon[] _weapons;

		private int _current;

		public WeaponsManager(BasicWeapon[] weapons)
		{
			_weapons = weapons;
		}

		public BasicWeapon ResetAndGetFirstWeapon()
		{
			_current = 0;
			return _weapons[_current];
		}
		public BasicWeapon GetCurrentWeapon()
		{
			return _weapons[_current];
		}

		public BasicWeapon GetNextWeapon()
		{
			
			if (_current < _weapons.Length - 1)
				_current++;

			return _weapons[_current];
		}
	}
}
﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Enemies
{
	[Serializable]
	public class AttractOtherEnemiesConfiguration : ModuleConfiguration
	{
		public float AttractMultiplier=0.1f;
		public float AttractRadius = 200f;
		public int AttractionIdentity;
	}
}
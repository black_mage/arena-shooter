﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Enemies
{
	[Serializable]
	public class EnemyModuleConfiguration
	{
		public RotateAndPursueTargetConfig RotateAndPursueTargetConfig;
		public RepelOtherEnemiesConfiguration RepelOtherEnemiesConfig;
		public AttractOtherEnemiesConfiguration AttractOtherEnemiesConfig;
		public ChaseTargetConfig ChaseTargetConfig;
		public ModuleConfiguration MoveTowardsTargetConfig;
		public ModuleConfiguration AvoidBulletsConfig;
		public ModuleConfiguration RoamLevelConfig;
	}
}
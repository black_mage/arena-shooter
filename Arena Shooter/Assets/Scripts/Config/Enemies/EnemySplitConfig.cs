﻿using System;
using Assets.Scripts.MonoBehaviours.Enemies;

namespace Assets.Scripts.Config.Enemies
{
	[Serializable]
	public class EnemySplitConfig
	{
		public BasicEnemy EnemyPrefab;
		public int Count;
	}
}
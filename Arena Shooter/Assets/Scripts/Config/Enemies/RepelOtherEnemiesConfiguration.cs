﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Enemies
{
	[Serializable]
	public class RepelOtherEnemiesConfiguration : ModuleConfiguration
	{
		public float RepelMultiplier=13;
		public float RepelRadius = 100f;
		public int RepulsionIdentity;
	}
}
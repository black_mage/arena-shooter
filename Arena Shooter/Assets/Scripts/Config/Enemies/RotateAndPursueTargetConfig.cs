﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Enemies
{
	[Serializable]
	public class RotateAndPursueTargetConfig : ModuleConfiguration
	{
		public float BurstLength = 3f;
	}
}
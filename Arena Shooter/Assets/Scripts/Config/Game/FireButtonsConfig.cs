﻿using System;
using UnityEngine;

namespace Assets.Scripts.Config.Game
{
	[Serializable]
	public class FireButtonsConfig
	{
		public KeyCode Up=KeyCode.UpArrow;
		public KeyCode Down=KeyCode.DownArrow;
		public KeyCode Left=KeyCode.LeftArrow;
		public KeyCode Right=KeyCode.RightArrow;
	}
}
﻿using System;
using Assets.Scripts.Data.Level;
using UnityEngine;

namespace Assets.Scripts.Config.Level
{
	[Serializable]
	public class LevelConfig
	{
		public Vector2 Dimensions;
		public Vector2 Density;
		public LevelMaterialEntry[] FloorMaterials;
		

		
	}
}
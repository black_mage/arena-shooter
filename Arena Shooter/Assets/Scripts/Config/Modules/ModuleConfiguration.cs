﻿using System;

namespace Assets.Scripts.Config.Modules
{
	[Serializable]
	public class ModuleConfiguration
	{
		public bool Enabled;
		public float Influence = 1f;
	}
}
﻿using System;
using Assets.Scripts.MonoBehaviours.Pickups;

namespace Assets.Scripts.Config.Pickups
{
	[Serializable]
	public class WeaponMutatorChance
	{
		public WeaponMutatorPickup Pickup;
		public int Chance;
	}
}
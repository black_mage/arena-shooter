﻿using System;
using Assets.Scripts.MonoBehaviours.Weapons;

namespace Assets.Scripts.Config.Pickups
{
	[Serializable]
	public class WeaponPickupCriteria
	{
		public BasicWeapon Weapon;
		public int Count;
	}
}
﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Config.Game;
using Assets.Scripts.MonoBehaviours.Weapons;

namespace Assets.Scripts.Config.Player
{
	[Serializable]
	public class GameConfiguration
	{
		public int Lives=3;
		public BasicWeapon[] Weapons;
		
		
	}
}
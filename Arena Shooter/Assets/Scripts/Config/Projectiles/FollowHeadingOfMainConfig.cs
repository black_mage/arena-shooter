﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class FollowHeadingOfMainConfig : ModuleConfiguration
	{
		public float TimeToTake;
		public bool StopWhenMainHeadingReached=true;
	}
}
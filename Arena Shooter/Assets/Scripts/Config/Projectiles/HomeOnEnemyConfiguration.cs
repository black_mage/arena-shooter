﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class HomeOnEnemyConfiguration : ModuleConfiguration
	{
		public bool FollowClosestEnemy=false;
	}
}
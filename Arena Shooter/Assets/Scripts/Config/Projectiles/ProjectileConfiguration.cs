﻿using System;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class ProjectileConfiguration
	{
		public float BaseSpeed=2500f;
		public float TurnSpeed=125f;
		public int Damage=10;
		public bool Bounce=true;
		public float BounceChance=0.2f;
	}
}
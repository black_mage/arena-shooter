﻿using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class ProjectileModuleConfiguration
	{
		public HomeOnEnemyConfiguration HomeOnEnemy;
		public FollowHeadingOfMainConfig FollowHeadingOfMain;
		public ZigZagConfiguration ZigZag;
	}
}
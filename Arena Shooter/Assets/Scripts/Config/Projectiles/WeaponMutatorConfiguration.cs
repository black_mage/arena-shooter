﻿using System;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class WeaponMutatorConfiguration
	{
		public float DamageMultiplier = 1f;
		public ProjectileConfiguration Config;
		public ProjectileModuleConfiguration Modules;
	}
}
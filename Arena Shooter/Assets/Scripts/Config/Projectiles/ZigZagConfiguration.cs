using System;
using Assets.Scripts.Config.Modules;

namespace Assets.Scripts.Config.Projectiles
{
	[Serializable]
	public class ZigZagConfiguration : ModuleConfiguration
	{
		public float Angle = 5f;
		public float TimeToTake = 0.1f;
	}
}
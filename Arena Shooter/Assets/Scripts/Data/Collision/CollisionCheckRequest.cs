﻿using UnityEngine;

namespace Assets.Scripts.Data.Collision
{
	public class CollisionCheckRequest
	{
		public Vector2 OldPosition { get; private set; }
		public Vector2 NewPosition { get; private set; }
		public RectBounds RectBounds { get; private set; }

		public float Radius { get; private set; }


		public CollisionCheckRequest(Vector3 newPosition, Vector3 oldPosition, float radius, RectBounds rectBounds)
		{
			Radius = radius;
			RectBounds = rectBounds;
			NewPosition = new Vector2(newPosition.x,newPosition.z);
			OldPosition = new Vector2(oldPosition.x,oldPosition.z);
		}
	}
}
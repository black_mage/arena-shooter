﻿namespace Assets.Scripts.Data.Collision
{
	public enum CollisionLineOrientation
	{
		Horizontal,
		Vertical,
		None
	}
}
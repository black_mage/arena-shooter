﻿namespace Assets.Scripts.Data.Collision
{
	public class CollisionResponse
	{
		public bool CollisionFound { get; private set; }
		public CollisionLineOrientation LineOrientation { get; private set; }

		public CollisionResponse(bool collisionFound, CollisionLineOrientation lineOrientation)
		{
			CollisionFound = collisionFound;
			LineOrientation = lineOrientation;
		}
	}
}
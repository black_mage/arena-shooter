﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data.Collision
{
	public class ParticleCollisionPlanes
	{
		private IEnumerable<Transform> _planes;

		public IEnumerable<Transform> Planes { get { return _planes; } }

		public ParticleCollisionPlanes(IEnumerable<Transform> planes)
		{
			_planes = planes;
		}
	}
}
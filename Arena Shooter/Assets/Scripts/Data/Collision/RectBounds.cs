﻿using UnityEngine;

namespace Assets.Scripts.Data.Collision
{
	public class RectBounds
	{
		public float X { get; private set; }
		public float Y { get; private set; }
		public float Width { get; private set; }
		public float Height { get; private set; }

		public float Top { get { return Y + Height; } }
		public float Bottom { get { return Y; } }
		public float Right { get { return X + Width; } }
		public float Left { get { return X; } }

		public float CentreX
		{
			get { return X + (Width/2); }
			
		}

		public float CentreY
		{
			get { return Y + (Height/2); }
		}


		public RectBounds(float x, float y, float width, float height)
		{
			Height = height;
			Width = width;
			X = x;
			Y = y;
		}

		public bool IsPointInRectXZ(Vector3 position)
		{
			return position.x > Left && position.x < Right && position.z > Bottom && position.z < Top;

		}
	}
}
﻿using Assets.Scripts.Abstract.Enemies;

namespace Assets.Scripts.Data.Enemies
{
	public class ModuleEntry<TModuleType>
	{
		public TModuleType Module { get; private set; }
		public float Influence { get; private set; }

		public ModuleEntry(TModuleType module, float influence)
		{
			Influence = influence;
			Module = module;
		}
	}
}
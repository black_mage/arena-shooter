﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Data.Game
{
	public class GameMetrics
	{
		public List<LifeMetric> LifeMetrics { get; private set; }  
		public int RemainingLives { get; set; }

		

		public GameMetrics()
		{
			LifeMetrics=new List<LifeMetric>();
		}
	}
}
﻿namespace Assets.Scripts.Data.Game
{
	public enum GameState
	{
		Spawning,
		Playing,
		Paused,
		Dead,
		GameOver
	}
}
﻿namespace Assets.Scripts.Data.Game
{
	public class LifeMetric
	{
		public float SecondsElapsed { get; set; }
		public int EnemiesKilled { get; set; }
	}
}
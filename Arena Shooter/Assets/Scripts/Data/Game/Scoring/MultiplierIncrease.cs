﻿using System;

namespace Assets.Scripts.Data.Game.Scoring
{
	[Serializable]
	public class MultiplierIncrease
	{
		public int AmountRequired;
		public int MultiplierIncreaseAmount;
		public bool Attained { get; set; }

		public MultiplierIncrease()
		{
			Attained = false;
		}
	}
}
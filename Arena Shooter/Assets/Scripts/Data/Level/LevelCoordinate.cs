﻿namespace Assets.Scripts.Data.Level
{
	public class LevelCoordinate
	{
		public int x { get; private set; }
		public int y { get; private set; }

		public LevelCoordinate(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}
}
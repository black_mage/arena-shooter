﻿using System;
using UnityEngine;

namespace Assets.Scripts.Data.Level
{
	[Serializable]
	public class LevelMaterialEntry
	{
		public Material Material;
		public Vector2 Tiling = new Vector2(1,1);

	}
}
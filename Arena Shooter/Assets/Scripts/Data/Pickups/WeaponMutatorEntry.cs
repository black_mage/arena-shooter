﻿using Assets.Scripts.Classes.Projectile.Mutators;

namespace Assets.Scripts.Data.Pickups
{
	public class WeaponMutatorEntry
	{
		public WeaponMutator Mutator { get; private set; }
		public float Life { get; private set; }

		public WeaponMutatorEntry(WeaponMutator mutator, float life)
		{
			Life = life;
			Mutator = mutator;
		}

		public void TakeLife(float amount)
		{
			Life -= amount;
		}
	}
}
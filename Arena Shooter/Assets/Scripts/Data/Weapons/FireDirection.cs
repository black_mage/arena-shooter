﻿namespace Assets.Scripts.Data.Weapons
{
	public enum FireDirection
	{
		None,
		Up,
		Down,
		Left,
		Right,
		UpLeft,
		UpRight,
		DownLeft,
		DownRight
	}
}
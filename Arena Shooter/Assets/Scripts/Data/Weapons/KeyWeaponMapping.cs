﻿using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.Data.Weapons
{
	public class KeyWeaponMapping
	{
		public KeyCode Key { get; private set; }
		public BasicWeapon Weapon { get; private set; }

		public KeyWeaponMapping(KeyCode key, BasicWeapon weapon)
		{
			Key = key;
			Weapon = weapon;
		}
	}
}
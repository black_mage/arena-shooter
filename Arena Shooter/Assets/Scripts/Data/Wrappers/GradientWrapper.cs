﻿using System;
using UnityEngine;

namespace Assets.Scripts.Data.Wrappers
{
	[Serializable]
	public class GradientWrapper
	{
		public Gradient Gradient;
	}
}
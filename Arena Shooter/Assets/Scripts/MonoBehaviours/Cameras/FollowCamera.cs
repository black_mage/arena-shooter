﻿using System.Linq;
using Assets.Scripts.Classes.Level;
using Assets.Scripts.Data.Collision;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Cameras
{
	[RequireComponent(typeof(Camera))]
	public class FollowCamera : MonoBehaviour
	{
		public float HoverHeight=1000;
		public float FollowSpeed=2;

		private RectBounds _movementBounds;
		private LevelManager _levelManager;
		private LevelInfo _levelInfo;
		public GameObject ToFollow;

		public bool Constrain = true;
		public float LevelEdgeAllowance = 40f;
		
		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			

			_levelInfo = _levelManager.LevelInfo;
			
			transform.position = new Vector3(ToFollow.transform.position.x, HoverHeight, ToFollow.transform.position.z);
			_movementBounds = _levelInfo.RectBounds;
		}

		public void Update()
		{
			
			if (ToFollow == null)
				return;

			
			var dest = new Vector3(ToFollow.transform.position.x, HoverHeight, ToFollow.transform.position.z);

			if(_movementBounds.IsPointInRectXZ(dest))
			{
				var oldPosition = transform.position;
				
				transform.position = Vector3.Lerp(transform.position, dest, FollowSpeed *Time.deltaTime);

				if (!Constrain)
					return;
				var xSides = new Vector3[2];
				var zSides = new Vector3[2];
				xSides[0] = GetComponent<Camera>().WorldToViewportPoint(new Vector3(_movementBounds.Left-LevelEdgeAllowance,0,transform.position.z));
				xSides[1] = GetComponent<Camera>().WorldToViewportPoint(new Vector3(_movementBounds.Right+LevelEdgeAllowance, 0, transform.position.z));
				zSides[0] = GetComponent<Camera>().WorldToViewportPoint(new Vector3(transform.position.x, 0, _movementBounds.Top+LevelEdgeAllowance));
				zSides[1] = GetComponent<Camera>().WorldToViewportPoint(new Vector3(transform.position.x, 0, _movementBounds.Bottom-LevelEdgeAllowance));

				var xSidesInScreen = xSides.Where(c => c.z > 0 && c.x > 0 && c.x < 1 && c.y > 0 && c.y < 1).Count();
				var zSidesInScreen = zSides.Where(c => c.z > 0 && c.x > 0 && c.x < 1 && c.y > 0 && c.y < 1).Count();

				if(xSidesInScreen > 0)
					transform.position = new Vector3(oldPosition.x,transform.position.y,transform.position.z);
				if(zSidesInScreen > 0)
					transform.position = new Vector3(transform.position.x,transform.position.y,oldPosition.z);
			}
		}

		
	}
}
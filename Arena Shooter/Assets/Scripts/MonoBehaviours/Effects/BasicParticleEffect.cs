﻿using Assets.Scripts.Abstract.Effects;
using Assets.Scripts.Data.Collision;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Effects
{
	
	public class BasicParticleEffect : PlayableEffect
	{
		public ParticleSystem ParticleSystem;

		private ParticleCollisionPlanes _collisionPlanes;



		public void StopEmittingAndKill()
		{
			ParticleSystem.Stop();
			Destroy(gameObject, ParticleSystem.startLifetime);
		}

		public override void Play()
		{
			transform.parent = null;
			ParticleSystem.Play();
			Destroy(this.gameObject,ParticleSystem.startLifetime + ParticleSystem.duration);
		}

		public void Pause()
		{
			ParticleSystem.Pause();
		}
	}
}
﻿

using System;
using System.Linq;
using Assets.Scripts.Abstract.Effects;
using Assets.Scripts.Data.Wrappers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MonoBehaviours.Effects
{
	[Serializable]
	[RequireComponent(typeof(MeshRenderer))]
	public class BasicShockwave : PlayableEffect
	{
		
		public float Duration;
		public float StartScale;
		public float[] EndScales;

		
		public GradientWrapper[] Gradients;
		
		public AnimationCurve AnimCurve;


		private float _timeLeft;

		private Material _shockwaveMaterial;

		private bool _active;
		private Gradient _gradient;
		private float _endScale;

		public void Awake()
		{
			
			transform.localScale = new Vector3(StartScale,0,StartScale);
			GetComponent<Renderer>().enabled = false;
			_timeLeft = Duration;

			_shockwaveMaterial = GetComponent<MeshRenderer>().material;

			_endScale = EndScales[Random.Range(0, EndScales.Count())];
		}

		public override void Play()
		{
			_active = true;
			transform.parent = null;
			Destroy(this.gameObject, Duration);
			GetComponent<Renderer>().enabled = true;

			var gradientIndex = Random.Range(0, Gradients.Count());
			_gradient = Gradients[gradientIndex].Gradient;

		}

		public void Update()
		{
			if (!_active)
				return;

			_timeLeft -= Time.deltaTime;
			var percentage = 1-(_timeLeft/Duration);

			var curve = AnimCurve.Evaluate(percentage);


			
			var scale = Mathf.Lerp(StartScale, _endScale, curve);

			transform.localScale = new Vector3(scale,1,scale);

			

			_shockwaveMaterial.color = _gradient.Evaluate(curve);
		}
	}
}
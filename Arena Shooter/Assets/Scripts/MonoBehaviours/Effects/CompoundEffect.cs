﻿using System.Linq;
using Assets.Scripts.Abstract.Effects;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Effects
{
	public class CompoundEffect : MonoBehaviour
	{
		public PlayableEffect[] Effects; 
		

		public void Play()
		{

			foreach(var effect in Effects)
			{
				((PlayableEffect)Instantiate(effect,transform.position,Quaternion.Euler(Vector3.up))).Play();
			}
			
		}
	}
}
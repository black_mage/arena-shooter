﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Effects
{
	public class LaserBoxEffect : MonoBehaviour
	{
		public LineRenderer Laser;
		public TrailRenderer Trail;
		public Color LaserColor;
		public float LaserWidth;

		private Vector3[] _nodes;
		private float[] _nodeDistances;

		private float _pathLength;
		public void SetLaserAttributes()
		{
			Laser.SetColors(LaserColor,LaserColor);
			Laser.SetWidth(LaserWidth,LaserWidth);
			
		}

		public void CreatePath(IEnumerable<Vector3> spawnPositions, float unitRadius)
		{
			var enumerable = spawnPositions as Vector3[] ?? spawnPositions.ToArray();
			var minX = enumerable.Select(p => p.x).Min();
			var minZ = enumerable.Select(p => p.z).Min();
			var maxX = enumerable.Select(p => p.x).Max();
			var maxZ = enumerable.Select(p => p.z).Max();

			_nodes = new Vector3[4];
			_nodeDistances = new float[4];
			_nodes[0] = new Vector3(minX - (Mathf.Sin(45*Mathf.Deg2Rad)*unitRadius),0,minZ - (Mathf.Cos(45*Mathf.Deg2Rad)*unitRadius));
			_nodes[1] = new Vector3(minX - (Mathf.Sin(135 * Mathf.Deg2Rad) * unitRadius), 0, maxZ - (Mathf.Cos(135 * Mathf.Deg2Rad) * unitRadius));

			_nodes[2] = new Vector3(maxX - (Mathf.Sin(225 * Mathf.Deg2Rad) * unitRadius), 0, maxZ - (Mathf.Cos(225 * Mathf.Deg2Rad) * unitRadius));
			_nodes[3] = new Vector3(maxX - (Mathf.Sin(315 * Mathf.Deg2Rad) * unitRadius), 0, minZ - (Mathf.Cos(315 * Mathf.Deg2Rad) * unitRadius));
			

			for(var n=0; n<4;n++)
			{
				var dist = (_nodes[n] - _nodes[(n + 1) % 4]).magnitude;
				_pathLength += dist;
				_nodeDistances[n] = dist;
			}

		}

		public void Interpolate(float delta)
		{
			var currentLength = Mathf.Lerp(0f, _pathLength, delta);

			var curDistance = 0f;
			var amountOnLine = 0f;
			var node = 0;
			for(var i=0; i < 4; i++)
			{
				

				var lineDistance = _nodeDistances[i];

				if (curDistance + lineDistance > currentLength)
				{
					node = i;
					amountOnLine = currentLength-curDistance;
					break;
				}
				
				curDistance += lineDistance;
			}

			transform.position = _nodes[node] + (_nodes[(node + 1)%4]-_nodes[node])*((amountOnLine)/_nodeDistances[node]);

			
				

		}


		public void OnDestroy()
		{
			
			Trail.gameObject.transform.parent = null;
		}
	}
}
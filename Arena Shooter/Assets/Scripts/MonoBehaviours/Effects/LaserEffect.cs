﻿using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Effects
{
	public class LaserEffect : MonoBehaviour
	{
		public LineRenderer Laser;
		public Light DecalLight;

		public Color StartColor = Color.white;
		public Color EndColor = Color.red;

		public float StartDecalAngle = 0f;
		public float EndDecalAngle = 120f;

		public float StartLaserWidth = 2f;
		public float EndLaserWidth = 15f;

		private Color _currentColor;
		private float _currentDecalAngle;
		private float _currentLaserWidth;
		public void Start()
		{
			_currentColor = StartColor;
			_currentDecalAngle = StartDecalAngle;
			_currentLaserWidth = StartLaserWidth;
			SetLaserAttributes();
		}

		public void Interpolate(float delta)
		{
			_currentColor = Color.Lerp(StartColor, EndColor, delta);
			_currentDecalAngle = Mathf.Lerp(StartDecalAngle, EndDecalAngle, delta);
			_currentLaserWidth = Mathf.Lerp(StartLaserWidth, EndLaserWidth, delta);
			SetLaserAttributes();
		}

		public void SetLaserAttributes()
		{
			Laser.SetColors(_currentColor,_currentColor);
			Laser.SetWidth(_currentLaserWidth,_currentLaserWidth);
			DecalLight.color = _currentColor;
			DecalLight.spotAngle = _currentDecalAngle;
		}
	}
}
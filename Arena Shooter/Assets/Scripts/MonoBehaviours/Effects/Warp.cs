﻿//File contributed by Carlos Wilkes. Many thanks indeed !

using UnityEngine;

[ExecuteInEditMode]
public class Warp : MonoBehaviour
{
	[Range(1, 1)]
	public int WarpIndex = 1;
	
	[Range(0.0f, 1000.0f)]
	public float WarpRadius = 2.0f;
	
	[Range(-1.0f, 1.0f)]
	public float WarpStrength = -1.0f;
	
	public Material WarpMaterial;
	
	protected virtual void LateUpdate()
	{
		if (WarpMaterial != null)
		{
			var baseName = "_Warp" + WarpIndex;
			
			WarpMaterial.SetVector(baseName + "_Position", transform.position);
			WarpMaterial.SetFloat (baseName + "_Radius"  , WarpRadius             );
			WarpMaterial.SetFloat (baseName + "_Strength", WarpStrength           );
		}
	}
	
	protected virtual void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transform.position, WarpRadius);
	}
}
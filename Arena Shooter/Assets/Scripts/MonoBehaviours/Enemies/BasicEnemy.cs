﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.Abstract.General;
using Assets.Scripts.Classes.Collision;
using Assets.Scripts.Classes.Enemies.Modules;
using Assets.Scripts.Classes.Scoring;
using Assets.Scripts.Config.Enemies;
using Assets.Scripts.Data.Collision;
using Assets.Scripts.Data.Enemies;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.General.Destroyables;
using Assets.Scripts.MonoBehaviours.Managers;
using Assets.Scripts.MonoBehaviours.Player;
using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Enemies
{
	[RequireComponent(typeof (Destroyable))]
	public class BasicEnemy : VelocityProvider
	{
		public string EnemyTypeName;
		public PointsSchedule PointsSchedule;
		public float BaseSpeed = 25;
		public float Dampener = 0.95f;

		
		public float BoundsCollisionRadius = 75f;

		public bool SpawnFacingThePlayer = true;
		
		public float TurnSpeed;

		public bool CanBeRepelledByDifferent = true;
		public bool CanBeRepelledBySame = true;
		public bool CanBeAttractedByOthers = true;



		public EnemyModuleConfiguration ModuleConfigurations;


		public EnemySplitConfig[] SplitIntoSmallerEnemiesOnDeath;

		public BasicParticleEffect SpawnEffect;
		public CompoundEffect DeathEffect;


		private EnemyManager _enemyManager;
		private LevelManager _levelManager;
		private PlayerBehaviour _player;
		private InGameStateManager _inGameStateManager;
		private ScoreManager _scoreManager;
		private PickupManager _pickupManager;
		private Vector3 _velocity;

		private List<ModuleEntry<IEnemyModule>> _modules;



		public Dictionary<BasicEnemy, float> EnemyDistancesSquared;

		public override Vector3 Velocity
		{
			get { return _velocity; }
		}

		public PlayerBehaviour TargetPlayer
		{
			get { return _player; }
		}


		public bool IsDead { get; private set; }

		public void Start()
		{

			IsDead = false;
			_levelManager = FindObjectOfType<LevelManager>();
			_enemyManager = FindObjectOfType<EnemyManager>();
			_player = FindObjectOfType<PlayerBehaviour>();
			_inGameStateManager = FindObjectOfType<InGameStateManager>();
			_scoreManager = FindObjectOfType<ScoreManager>();
			_pickupManager = FindObjectOfType<PickupManager>();
			
			_velocity = new Vector3(0, 0, 1);

			PointsSchedule.Start();

			EnemyDistancesSquared = new Dictionary<BasicEnemy, float>();

			SpawnEffect.Play();


			_modules = new List<ModuleEntry<IEnemyModule>>();

			if(ModuleConfigurations.RotateAndPursueTargetConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new RotateAndPursueTarget(this,ModuleConfigurations.RotateAndPursueTargetConfig.BurstLength),ModuleConfigurations.RotateAndPursueTargetConfig.Influence));
			}

			if(ModuleConfigurations.RepelOtherEnemiesConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new RepelOtherEnemies(this, ModuleConfigurations.RepelOtherEnemiesConfig), ModuleConfigurations.RepelOtherEnemiesConfig.Influence));
			}

			if(ModuleConfigurations.AttractOtherEnemiesConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new AttractOtherEnemies(this, ModuleConfigurations.AttractOtherEnemiesConfig), ModuleConfigurations.RepelOtherEnemiesConfig.Influence));
			}

			if(ModuleConfigurations.ChaseTargetConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new ChaseTarget(this,ModuleConfigurations.ChaseTargetConfig), ModuleConfigurations.ChaseTargetConfig.Influence));
			}

			if(ModuleConfigurations.MoveTowardsTargetConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new MoveTowardsPlayer(this),ModuleConfigurations.MoveTowardsTargetConfig.Influence ));
			}

			if(ModuleConfigurations.AvoidBulletsConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new AvoidPlayerBullets(this, 50f), ModuleConfigurations.AvoidBulletsConfig.Influence));
			}
			if(ModuleConfigurations.RoamLevelConfig.Enabled)
			{
				_modules.Add(new ModuleEntry<IEnemyModule>(new RoamLevel(this),ModuleConfigurations.RoamLevelConfig.Influence ));
			}


			if(SpawnFacingThePlayer)
				transform.LookAt(_player.transform);
		}



		public void Destroy(GameObject sender)
		{

			GetComponent<Destroyable>().Destroy(sender);
		}

		public void Update()
		{
			if (_inGameStateManager.CurrentState != GameState.Playing)
				return;

			EnemyDistancesSquared.Clear();
			foreach (var enemy in _enemyManager.Enemies.Where(e => e != this))
			{
				EnemyDistancesSquared.Add(enemy, (transform.position - enemy.transform.position).sqrMagnitude);
			}


			PointsSchedule.Update(Time.deltaTime);


			var target = _player.transform.position;
			var moduleVelocities = _modules.Select(e => e.Module.Process(target)*e.Influence).Aggregate((a, b) => a + b);

			_velocity += moduleVelocities;

			var oldPosition = transform.position;
			transform.position += _velocity;



			var request = new CollisionCheckRequest(transform.position, oldPosition, BoundsCollisionRadius,
			                                        _levelManager.LevelInfo.RectBounds);
			var col = BoundsCollisionChecker.CheckCollision(request);
			if (col.CollisionFound)
			{


				transform.position = oldPosition;


				if (col.LineOrientation == CollisionLineOrientation.Horizontal)
					_velocity = new Vector3(Velocity.x, Velocity.y, -1*Velocity.z);
				else
					_velocity = new Vector3(-1*Velocity.x, Velocity.y, Velocity.z);

			}

			_velocity *= Dampener;



		}

		public void OnProjectileHit(BasicProjectile projectile)
		{
			_velocity = projectile.Velocity*0.005f;
		}

		public void OnHit(GameObject sender)
		{
			_velocity = Vector3.zero;
		}

		public void OnDestroyed(GameObject sender)
		{
			
			
			if (sender != _enemyManager.gameObject)
			{
				foreach (var entry in SplitIntoSmallerEnemiesOnDeath)
				{
					for (var c = 0; c < entry.Count; c++)
						_enemyManager.InstantSpawnEnemy(entry.EnemyPrefab, transform.position);
				}

				_pickupManager.HandleEnemyDeath(transform);
				_inGameStateManager.Metrics.LifeMetrics.Last().EnemiesKilled++;

				_scoreManager.EnemyDeath(this);
			}
			
			IsDead = true;
			_enemyManager.RemoveEnemy(this);
			((CompoundEffect) GameObject.Instantiate(DeathEffect,transform.position,transform.rotation)).Play();
			
			

			GameObject.Destroy(gameObject);
			 
		}

		public void AddVelocity(Vector3 velocity)
		{
			_velocity += velocity;
		}
	}
}
﻿using System.Collections.Generic;
using Assets.Scripts.Classes.Enemies.Groups;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Enemies.Spawners
{
	public class EnemyGroupSpawner : MonoBehaviour
	{
		public BasicEnemy EnemyPrefab;
		
		public Vector2 Amount;
		public Vector2 Offsets;
		public float CoolDown = 5;
		public float StartDelay = 0f;
		public LaserBoxEffect BoxEffect;

		private InGameStateManager _gameStateManager;
		private LevelManager _levelManager;
		private EnemyManager _enemyManager;
		private float _timeUntilNextSpawn;

		public void Start()
		{
			_gameStateManager = FindObjectOfType<InGameStateManager>();
			_levelManager = FindObjectOfType<LevelManager>();
			_enemyManager = FindObjectOfType<EnemyManager>();
			_timeUntilNextSpawn = StartDelay;
		}

		public void Reset()
		{
			_timeUntilNextSpawn = StartDelay;
		}

		public void Update()
		{
			if (_gameStateManager.CurrentState != GameState.Playing)
				return;

			if(_timeUntilNextSpawn  <= 0f)
			{
				var xMin = _levelManager.LevelInfo.RectBounds.Left + EnemyPrefab.BoundsCollisionRadius*2;
				var zMin = _levelManager.LevelInfo.RectBounds.Bottom + EnemyPrefab.BoundsCollisionRadius*2;
				var xMax = _levelManager.LevelInfo.RectBounds.Right -(((Amount.x - 1)*Offsets.x) + EnemyPrefab.BoundsCollisionRadius*2);
				var zMax = _levelManager.LevelInfo.RectBounds.Top - (((Amount.y-1)*Offsets.y)+EnemyPrefab.BoundsCollisionRadius*2);

				var startPosition = new Vector3(Random.Range(xMin, xMax), 0, Random.Range(zMin, zMax));

				var xPos = startPosition.x;
				var zPos = startPosition.z;

				var positions = new LinkedList<Vector3>();
				for (var x = 0; x < Amount.x; x++)
				{
					for (var z = 0; z < Amount.y; z++)
					{
						positions.AddLast(new Vector3(xPos, 40, zPos));
				
						zPos += Offsets.y;
					}
					zPos = startPosition.z;
					xPos += Offsets.x;
				}

				_enemyManager.AddSpawn(new EnemyGroupSpawn(EnemyPrefab,1.5f,BoxEffect,positions));
				_timeUntilNextSpawn = CoolDown;
			}
			else
			{
				_timeUntilNextSpawn -= Time.deltaTime;
			}
		}
	}
}
﻿using Assets.Scripts.Classes.Enemies;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Enemies.Spawners
{
	public class EnemySpawner : MonoBehaviour
	{
		public BasicEnemy EnemyType;

		public float Cooldown = 5;

		public LaserEffect LaserEffect;

		private LevelManager _levelManager;
		private InGameStateManager _gameStateManager;
		private float _timeUntilNextSpawn;

		private EnemyManager _enemyManager;
		

		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			_gameStateManager = FindObjectOfType<InGameStateManager>();
			_enemyManager = FindObjectOfType<EnemyManager>();
		}


		public void Update()
		{
			if (_gameStateManager.CurrentState != GameState.Playing)
				return;

			if (_timeUntilNextSpawn <= 0f)
			{
				var position =
					new Vector3(Random.Range(_levelManager.LevelInfo.RectBounds.Left+EnemyType.BoundsCollisionRadius, _levelManager.LevelInfo.RectBounds.Right- EnemyType.BoundsCollisionRadius), 40,
								Random.Range(_levelManager.LevelInfo.RectBounds.Bottom+EnemyType.BoundsCollisionRadius, _levelManager.LevelInfo.RectBounds.Top+EnemyType.BoundsCollisionRadius));
				



				_enemyManager.AddSpawn(new EnemySpawn(EnemyType,LaserEffect,1.5f,position) );

				_timeUntilNextSpawn = Cooldown;
			}
			else
				_timeUntilNextSpawn -= Time.deltaTime;
		}

	}
}
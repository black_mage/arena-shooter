﻿using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.General.Destroyables
{
	public class Destroyable : MonoBehaviour
	{
		public int Health = 100;
		public int MaxHealth;

		public bool IsDead { get; private set; }


		public void Start()
		{
			IsDead = false;

		}

		public void TakeProjectileDamage(BasicProjectile projectile)
		{
			if (IsDead)
				return;
			Health -= projectile.Config.Damage;

			SendMessage("OnProjectileHit",projectile,SendMessageOptions.DontRequireReceiver);

			if(Health <=0)
			{
				Destroy(this.gameObject);
			}
		}

		public void TakeDamage(int damage)
		{
			if (IsDead)
				return;
			Health -= damage;

			SendMessage("OnHit",gameObject, SendMessageOptions.DontRequireReceiver);

			if (Health <= 0)
			{
				Destroy(this.gameObject);
			}
			
				
			
		}

		public void Destroy(GameObject sender)
		{
			
			IsDead = true;
			SendMessage("OnDestroyed", sender, SendMessageOptions.DontRequireReceiver);
		}
	}
}
﻿using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.General.Destroyables
{
	[RequireComponent(typeof(MeshFilter))]
	public class DestroyableColorChanger : MonoBehaviour
	{
		public Destroyable Destroyable;
		public Gradient DamageGradient;
		public MeshFilter MeshToChange;

		public string ColorName;
		public bool UseColorName = false;

		public void Update()
		{
			var damagePercent = 1f-((float)Destroyable.Health/(float)Destroyable.MaxHealth);

			var color = DamageGradient.Evaluate(damagePercent);


			if (UseColorName)
				MeshToChange.GetComponent<Renderer>().material.SetColor(ColorName, color);
			else
				MeshToChange.GetComponent<Renderer>().material.color = color;
			



		}
	}
}
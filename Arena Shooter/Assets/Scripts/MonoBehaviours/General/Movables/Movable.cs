﻿using System;
using Assets.Scripts.Abstract.General;
using Assets.Scripts.Classes.Collision;
using Assets.Scripts.Data.Collision;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.General.Movables
{
	public class Movable : VelocityProvider
	{
		public float BaseSpeed=45;
		public float VelocityDampener = .94f;
		public float CollisionRadius = 20f;
		
		public bool Collides;

		
		private Vector3 _velocity;

		public override Vector3 Velocity { get { return _velocity; } }

		

		public event Action<CollisionResponse> OnCollision;
		public event Action<CollisionCheckRequest> CollisionDetectionHandler;
		

		private RectBounds _bounds;
		

		public void Move(Vector3 movement)
		{
			_velocity += movement * BaseSpeed;
			_velocity.Set(_velocity.x,0f,_velocity.z);
			

		}

		public void SetBounds(RectBounds bounds)
		{
			_bounds = bounds;
		}
		public void Stop()
		{
			_velocity = Vector3.zero;
		}
		public void Update()
		{
			
			if (_velocity == Vector3.zero)
				return;

			var oldPosition = transform.position;
			
			transform.position += (_velocity*Time.deltaTime);

			_velocity *= VelocityDampener;

			if (!Collides)
				return;


			var request = new CollisionCheckRequest(transform.position, oldPosition, CollisionRadius, _bounds);
			
			
			if(CollisionDetectionHandler !=null)
			{
				
				CollisionDetectionHandler(request);
				return;
			}

			if (_bounds == null)
				return;
			var col = BoundsCollisionChecker.CheckCollision(request);

			if (col.CollisionFound)
			{
				if (OnCollision !=null)
					OnCollision(col);

				transform.position = oldPosition;

				if (col.LineOrientation == CollisionLineOrientation.Horizontal)
					_velocity = new Vector3(_velocity.x, _velocity.y, -1 * _velocity.z);
				else
					_velocity = new Vector3(-1 * _velocity.x, _velocity.y, _velocity.z);
			}
		}
	}
}
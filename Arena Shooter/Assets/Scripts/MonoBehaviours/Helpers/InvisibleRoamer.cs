﻿using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Code
{
	public class InvisibleRoamer : MonoBehaviour
	{
		[SerializeField] private LevelManager _levelManager;
		[SerializeField] private float _radius = 20f;
		[SerializeField] private float _moveSpeed = 5f;

		private Vector3 _destination;
		

		public void Start()
		{
			if (_levelManager == null)
			{
				Debug.Log("No level manager has been set for Invisible Roamer.");
				return;
			}

			PickDestination();
		}

		public void Update()
		{

			if(Vector3.SqrMagnitude(transform.position-_destination)<_radius*_radius)
				PickDestination();
			

			transform.position = Vector3.MoveTowards(transform.position, _destination, _moveSpeed*Time.deltaTime);


		}

		private void PickDestination()
		{
			var bounds = _levelManager.LevelInfo.RectBounds;

			var x = Random.Range(bounds.Left + _radius, bounds.Right - _radius);
			var z = Random.Range(bounds.Bottom + _radius, bounds.Top - _radius);
			_destination = new Vector3(x, 0, z);
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abstract.Enemies;
using Assets.Scripts.Classes.Enemies;
using Assets.Scripts.MonoBehaviours.Enemies;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class EnemyManager : MonoBehaviour
	{
		private LinkedList<BasicEnemy> _enemies;
		private LinkedList<IEnemySpawn> _spawns; 

		public void Start()
		{
			_enemies = new LinkedList<BasicEnemy>();
			_spawns = new LinkedList<IEnemySpawn>();
		}

		public IEnumerable<BasicEnemy> Enemies { get { return _enemies; } } 

		public void AddEnemy(BasicEnemy enemy)
		{
			_enemies.AddLast(enemy);
		}

		public void RemoveEnemy(BasicEnemy enemy)
		{
			_enemies.Remove(enemy);
		}

		public void AddSpawn(IEnemySpawn spawn)
		{
			_spawns.AddLast(spawn);
			spawn.OnSpawned += SpawnSpawned;
		}

		public void RemoveSpawn(EnemySpawn spawn)
		{
			_spawns.Remove(spawn);
			spawn.OnSpawned -= SpawnSpawned;
		}

		public void InstantSpawnEnemy(BasicEnemy enemy, Vector3 position)
		{
			var newEnemy = (BasicEnemy)Object.Instantiate(enemy, position, Quaternion.identity);
			AddEnemy(newEnemy);
		}

		public void DestroyAllEnemies()
		{
			foreach(var enemy in _enemies.ToArray())
				enemy.Destroy(this.gameObject);

			_enemies.Clear();

			foreach (var spawn in _spawns.ToArray())
				spawn.Destroy();

			_spawns.Clear();
		}


		public void SpawnSpawned(IEnemySpawn spawn,BasicEnemy spawnedEnemy)
		{
			_spawns.Remove(spawn);
			_enemies.AddLast(spawnedEnemy);
		}
		public void Update()
		{
			foreach(var spawn in _spawns.ToArray())
			{
				spawn.Update();
			}
		}
	}
}
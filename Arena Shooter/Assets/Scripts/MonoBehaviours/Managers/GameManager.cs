﻿using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class GameManager : MonoBehaviour
	{

		private InGameStateManager _inGameStateManager;

		
		public void Awake()
		{
			
			Application.targetFrameRate = 60;
			
			DontDestroyOnLoad(this);
		}

		public void Start()
		{
			
			_inGameStateManager = FindObjectOfType<InGameStateManager>();
			_inGameStateManager.BeginLevel();
		}

		

		public void Update()
		{


		}
	}
}

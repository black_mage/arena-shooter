﻿using Assets.Scripts.MonoBehaviours.Player;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class GameManagerInstance : MonoBehaviour
	{
		private static GameManager _instance;

		public GameManager Instance
		{
			get
			{
				if(_instance == null)
				{
					var manager = new GameObject();
					_instance = manager.AddComponent<GameManager>();
				}
				return _instance;
			}
		}

		public void Start()
		{
			_instance = Instance;
		}
	}
}
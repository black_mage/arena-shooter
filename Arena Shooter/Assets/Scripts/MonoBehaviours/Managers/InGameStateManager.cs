﻿using System.Linq;
using Assets.Scripts.Classes.Weapons;
using Assets.Scripts.Config.Player;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Enemies.Spawners;
using Assets.Scripts.MonoBehaviours.Pickups;
using Assets.Scripts.MonoBehaviours.Player;
using Assets.Scripts.MonoBehaviours.Projectiles;
using Assets.Scripts.MonoBehaviours.UI;
using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class InGameStateManager : MonoBehaviour
	{

		

		private PlayerSpawner _playerSpawner;
		private GameState _currentState;
		private PlayerBehaviour _player;
		private EnemyManager _enemyManager;
		private PickupManager _pickupManager;


		private LivesCounter _livesCounter;
		private TimeCounter _timeCounter;
		public GameState CurrentState { get { return _currentState; } }

		public GameMetrics Metrics { get; private set; }

		public GameConfiguration Config;

		public BasicWeapon CurrentWeapon {get {return _currentWeapon ?? (_currentWeapon = Config.Weapons[0]); }}

		public CanvasGroup PauseScreen;

		private BasicWeapon _currentWeapon;

		private WeaponsManager _weaponsManager;

		public void Awake()
		{
			_currentState = GameState.Spawning;
			_weaponsManager = new WeaponsManager(Config.Weapons);
		}

		public void Start()
		{
			_playerSpawner = FindObjectOfType<PlayerSpawner>();
			_playerSpawner.OnPlayerSpawned += OnPlayerSpawn;
			
			_enemyManager = FindObjectOfType<EnemyManager>();
			_pickupManager = FindObjectOfType<PickupManager>();
			_livesCounter = FindObjectOfType<LivesCounter>();
			_timeCounter = FindObjectOfType<TimeCounter>();

			PauseScreen.alpha = 0;

			Metrics = new GameMetrics();
			Metrics.RemainingLives = Config.Lives;
			_livesCounter.SetLivesDisplay(Metrics.RemainingLives);
		}

		public void BeginLevel()
		{
			_currentState = GameState.Spawning;
			_playerSpawner.BeginSpawn();
		}

		public void Update()
		{
			if (_currentState == GameState.Paused)
			{
				if (Input.GetButtonDown("Start"))
				{
					_currentState = GameState.Playing;
					PauseScreen.alpha = 0;
					Time.timeScale = 1;
					return;
				}
			}
			if (_currentState == GameState.Playing)
			{
				Metrics.LifeMetrics.Last().SecondsElapsed += Time.deltaTime;
				_timeCounter.SetTime(Metrics.LifeMetrics.Last().SecondsElapsed);

				if(Input.GetButtonDown("Start") && Metrics.LifeMetrics.Last().SecondsElapsed>1)
				{

					Time.timeScale = 0;
					_currentState = GameState.Paused;
					PauseScreen.alpha = 1;
				}
			}



		}



		private void OnPlayerDeath(PlayerBehaviour player)
		{


			
			_enemyManager.DestroyAllEnemies();
			DestroyObjectsOfType<Pickup>();
			DestroyObjectsOfType<BasicProjectile>();


			_pickupManager.Reset();


			Metrics.RemainingLives--;
			_livesCounter.SetLivesDisplay(Metrics.RemainingLives);
			if (Metrics.RemainingLives > 0)
			{
				_currentState = GameState.Spawning;
				_playerSpawner.BeginSpawn();
			}
			else
			{
				_currentState = GameState.GameOver;
				_currentWeapon = _weaponsManager.ResetAndGetFirstWeapon();
			}




		}

		public void ChangeWeapon(BasicWeapon weapon)
		{
			_currentWeapon = weapon;
			_player.ChangeWeapon(weapon);
		}

		public void NextWeapon()
		{
			_currentWeapon = _weaponsManager.GetNextWeapon();
			_player.ChangeWeapon(_currentWeapon);
		}

		private void OnPlayerSpawn(PlayerBehaviour player)
		{
			
			_currentState = GameState.Playing;
			_player = player;

			_player.OnPlayerDeath += OnPlayerDeath;

			Metrics.LifeMetrics.Add(new LifeMetric());

		
		}


		private void DestroyObjectsOfType<TType>() where TType: MonoBehaviour
		{
			foreach(var gameObject in FindObjectsOfType<TType>())
			{					
				Destroy(gameObject.gameObject);
			}

		}
	}
}
﻿using Assets.Scripts.Classes.Level;
using Assets.Scripts.Config.Level;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	
	public class LevelManager : MonoBehaviour
	{
		public LevelConfig Config;

		public LevelInfo LevelInfo { get; private set; }



		public void Awake()
		{
			LevelInfo = new LevelBuilder().BuildLevel(Config);
		}


	}
}
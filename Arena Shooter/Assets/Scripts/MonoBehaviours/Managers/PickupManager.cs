﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Config.Pickups;
using Assets.Scripts.Data.Game;
using Assets.Scripts.Data.Pickups;
using Assets.Scripts.MonoBehaviours.Pickups;
using Assets.Scripts.MonoBehaviours.Player;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class PickupManager : MonoBehaviour
	{
		public WeaponPickup PickupPrefab;
		public List<WeaponPickupCriteria> WeaponPickups;
		public float ChanceOfMutatorPickup = 0.05f;
		public float ChanceOfMutatorPickupDegradation = 0.6f;
		public List<WeaponMutatorChance> MutatorPickups;
		

		public Gradient PickupLifetimeColors;

		private InGameStateManager _stateManager;
		private LevelManager _levelManager;
		private int _nextPickupIndex;
		private bool _pickupsExhausted;

		private PlayerBehaviour _player;

		private List<WeaponMutatorPickup> _mutatorSelectionList;

		

		public void Start()
		{
			_stateManager = FindObjectOfType<InGameStateManager>();
			_levelManager = FindObjectOfType<LevelManager>();
			
			_mutatorSelectionList = new List<WeaponMutatorPickup>();
			foreach(var item in MutatorPickups)
			{
				for(var p =0; p < item.Chance;p++)
					_mutatorSelectionList.Add(item.Pickup);
			}
		}

		public void Update()
		{
			if (_stateManager.CurrentState != GameState.Playing)
				return;
			if (_pickupsExhausted)
				return;
			if(_stateManager.Metrics.LifeMetrics.Last().EnemiesKilled == WeaponPickups[_nextPickupIndex].Count)
			{
				var position =
					new Vector3(Random.Range(_levelManager.LevelInfo.RectBounds.Left, _levelManager.LevelInfo.RectBounds.Right), 0,
					            Random.Range(_levelManager.LevelInfo.RectBounds.Bottom, _levelManager.LevelInfo.RectBounds.Top));

				var pickup = (WeaponPickup)Instantiate(PickupPrefab, position, Quaternion.identity);
				pickup.Weapon = WeaponPickups[_nextPickupIndex].Weapon;

				_nextPickupIndex++;

				if (_nextPickupIndex == WeaponPickups.Count)
					_pickupsExhausted = true;
			}
		}

		public void Reset()
		{
			_nextPickupIndex = 0;
			_pickupsExhausted = false;
			foreach(var pickup in FindObjectsOfType<WeaponPickup>())
			{
				Destroy(pickup.gameObject);
			}
		}


		public void HandleEnemyDeath(Transform transform)
		{
			_player = FindObjectOfType<PlayerBehaviour>();
			var mutatorCount = _player.GetActiveMutators().Count();
			var chance = ChanceOfMutatorPickup*(Mathf.Pow(ChanceOfMutatorPickupDegradation,mutatorCount));
			if (Random.Range(0f, 1f) > chance)
				return;

			var pickup = _mutatorSelectionList[Random.Range(0, _mutatorSelectionList.Count)];

			GameObject.Instantiate(pickup, transform.position, transform.rotation);
		}
	}
}
﻿using System;
using System.Linq;
using Assets.Scripts.Data.Game;
using Assets.Scripts.Data.Game.Scoring;
using Assets.Scripts.MonoBehaviours.Enemies;
using Assets.Scripts.MonoBehaviours.Player;
using Assets.Scripts.MonoBehaviours.UI;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Managers
{
	public class ScoreManager : MonoBehaviour
	{
		[SerializeField]
		private ScoreUI _scoreUI;

		[SerializeField]
		private MultiplierIncrease[] _killsMultiplierIncrease;

		[SerializeField] private MultiplierIncrease[] _timeMultiplierIncrease;

		
		private ulong _score = 0ul;

		private ulong _multiplier = 1;

		private InGameStateManager _inGameStateManager;
		private PopupManager _popupManager;
		private int _lastKillsRequired;


		private PlayerBehaviour _currentPlayer;

		public void Start()
		{
			_inGameStateManager = FindObjectOfType<InGameStateManager>();
			_popupManager = FindObjectOfType<PopupManager>();
		}

		public void Update()
		{
			if (_inGameStateManager.CurrentState != GameState.Playing)
				return;
			var metrics = _inGameStateManager.Metrics;

			var nextKillsTarget = _killsMultiplierIncrease.FirstOrDefault(k => k.Attained!=true);

			if(nextKillsTarget != null)
			{
			
				if(metrics.LifeMetrics.Last().EnemiesKilled>=nextKillsTarget.AmountRequired)
				{
					_multiplier += (ulong) nextKillsTarget.MultiplierIncreaseAmount;
					nextKillsTarget.Attained = true;
					_lastKillsRequired = nextKillsTarget.AmountRequired;
				}
			}
			else
			{
				if(metrics.LifeMetrics.Last().EnemiesKilled>=_lastKillsRequired*2)
				{
					_multiplier++;
					_lastKillsRequired *= 2;
				}
			}

			var nextTimeTarget = _timeMultiplierIncrease.FirstOrDefault(k => k.Attained != true);
			if(metrics.LifeMetrics.Last().SecondsElapsed>=nextTimeTarget.AmountRequired)
			{
				_multiplier += (ulong) nextTimeTarget.MultiplierIncreaseAmount;
				nextTimeTarget.Attained = true;
			}



			var newPlayer = FindObjectOfType<PlayerBehaviour>();
			if(newPlayer != _currentPlayer)
			{
				_currentPlayer = newPlayer;
				_currentPlayer.OnPlayerDeath += OnPlayerDeath;
			}


			_scoreUI.SetScore(_score,_multiplier);
		}

		public void EnemyDeath(BasicEnemy enemy)
		{
			var value = Convert.ToUInt64(enemy.PointsSchedule.BaseScore);
			value += Convert.ToUInt64(enemy.PointsSchedule.GetBonusValue());
			value *= _multiplier;
			_popupManager.Popup(enemy.transform.position,value.ToString());
			_score += value;


		}

		public void OnPlayerDeath(PlayerBehaviour player)
		{
			_multiplier = (ulong)Mathf.Max(_multiplier/2,1);
			_scoreUI.SetScore(_score,_multiplier);
			foreach(var t in _timeMultiplierIncrease)
			{
				t.Attained = false;
			}
			foreach(var t in _killsMultiplierIncrease)
			{
				t.Attained = false;
			}
		}
	}
}
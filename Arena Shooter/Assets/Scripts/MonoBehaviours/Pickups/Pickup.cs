﻿using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Pickups
{
	public class Pickup : MonoBehaviour
	{
		public bool Expires = true;
		public float Lifetime = 20f;

		private float _timeLeft;

		public void Start()
		{
			_timeLeft = Lifetime;
		}

		public void Update()
		{
			if (!Expires)
				return;
			_timeLeft -= Time.deltaTime;

			
				
			SendMessage("AlterPickupColor",1-(_timeLeft/Lifetime),SendMessageOptions.DontRequireReceiver);

			if (_timeLeft < 0)
				Destroy(this.gameObject);
		}
	}
}
﻿using Assets.Code;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Pickups
{
	
	public class PickupAnimation : MonoBehaviour
	{
		public float StartScale = 1f;
		public float EndScale = 2f;
		public float ScaleSpeed = 0.5f;
		public float TurnSpeed = 10f;

		private bool _scaleUp;
		private bool _firstScaleUp;

		private PickupManager _pickupManager;
		public void Start()
		{
			_scaleUp = true;
			_firstScaleUp = true;
			transform.localScale = new Vector3(0,transform.localScale.y,0);


			_pickupManager = FindObjectOfType<PickupManager>();
		}

		public void Update()
		{
			transform.Rotate(0f,TurnSpeed*Time.deltaTime,0f);
			var scale = transform.localScale.x;

			if (_scaleUp)
			{
				scale += (ScaleSpeed*(_firstScaleUp ? 5f : 1f))*Time.deltaTime;
				if (scale >= EndScale)
				{
					_scaleUp = false;
					_firstScaleUp = false;
				}
			}
			else
			{
				scale -= ScaleSpeed * Time.deltaTime;
				if (scale <= StartScale)
					_scaleUp = true;
			}

			transform.localScale = new Vector3(scale,transform.localScale.y,scale);

		}


		public void AlterPickupColor(object value)
		{
			var percent = (float) value;
			
			var renderers = gameObject.GetComponentsInChildren<MeshRenderer>();

			foreach (var renderer in renderers)
			{
				renderer.material.color = _pickupManager.PickupLifetimeColors.Evaluate(percent);
			}
		}
	}
}
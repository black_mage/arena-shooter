﻿using Assets.Scripts.Classes.Projectile.Mutators;
using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.MonoBehaviours.Player;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Pickups
{
	public class WeaponMutatorPickup : Pickup
	{
		public float Radius;
		public float Duration;

		
		public WeaponMutatorConfiguration config;
		
		private PlayerBehaviour _player;


		public new void Update()
		{
			if (_player == null)
				_player = FindObjectOfType<PlayerBehaviour>();

			if (_player == null)
				return;
			if (Vector3.Distance(_player.transform.position, transform.position) < Radius)
			{

				_player.AddMutator(new WeaponMutator(config), Duration);
				
				GameObject.Destroy(this.gameObject);
			}

			base.Update();
		}
	}
}
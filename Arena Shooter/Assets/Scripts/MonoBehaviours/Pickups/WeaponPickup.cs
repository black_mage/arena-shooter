﻿using Assets.Scripts.MonoBehaviours.Managers;
using Assets.Scripts.MonoBehaviours.Player;
using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Pickups
{

	public class WeaponPickup : Pickup
	{
		public BasicWeapon Weapon;
		public float Radius;

		private PlayerBehaviour _player;

		private InGameStateManager _gameStateManager;


		public void Awake()
		{
			_gameStateManager = FindObjectOfType<InGameStateManager>();
		}
		public new void Update()
		{
			
			if (_player == null)
				_player = FindObjectOfType<PlayerBehaviour>();

			if (_player == null)
				return;
			if(Vector3.Distance(_player.transform.position,transform.position)<Radius)
			{
				
				_gameStateManager.NextWeapon();
				GameObject.Destroy(this.gameObject);
			}

			base.Update();
		}

	}
}
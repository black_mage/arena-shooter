﻿using Assets.Scripts.MonoBehaviours.General.Movables;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Player.Effects.Rotation
{
	public class RingPitcher : MonoBehaviour
	{
		public Movable Movable;

		public float MaxPitchAngle = 25;
		public Vector3 VelocityOfMaxBank;
		
		public Space RotationSpace = Space.Self;

		private Vector3 _turnSpeed;
		
		public void Start()
		{
			if (Movable == null)
				Debug.Log("You didn't provide a movable for the ring rotator.");
		}

		public void FixedUpdate()
		{
			var oldZ = transform.rotation.eulerAngles.z;
			var oldX = transform.rotation.eulerAngles.x;
			var destZ =(Movable.Velocity.x / VelocityOfMaxBank.x)*MaxPitchAngle * -1;
			var destX = (Movable.Velocity.z/VelocityOfMaxBank.z)*MaxPitchAngle;

			var newZ = Mathf.LerpAngle(oldZ, destZ, Mathf.Abs(Movable.Velocity.x/VelocityOfMaxBank.x));
			var newX = Mathf.LerpAngle(oldX, destX, Mathf.Abs(Movable.Velocity.z/VelocityOfMaxBank.x));

			var diffZ = newZ - oldZ%360;
			var diffX = newX - oldX%360;

			
			transform.Rotate(diffX,0f,diffZ,RotationSpace);
			
		}
	}
}
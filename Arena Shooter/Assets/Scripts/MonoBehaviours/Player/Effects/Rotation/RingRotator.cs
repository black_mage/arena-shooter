﻿using Assets.Scripts.Abstract.General;
using Assets.Scripts.MonoBehaviours.General.Movables;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Player.Effects.Rotation
{
	public class RingRotator : MonoBehaviour
	{
		public VelocityProvider Movable;

		public Vector3 Axis;

		
		
		public float RotationDampener = 0.9f;

		
		public float VelocityMultiplier = 0.1f;

		public Space RotationSpace = Space.Self;
		

		private float _turnSpeed;

		public void Start()
		{
			if (Movable == null)
				Debug.Log("You didn't provide a movable for the ring rotator.");
		}

		public void Update()
		{
			var sqrMagnitude = Movable.Velocity.sqrMagnitude;
			var turnAngle = Mathf.Atan2(Movable.Velocity.x,Movable.Velocity.z);

			var clockwise = !(turnAngle < -1);


			var speed = clockwise ? sqrMagnitude : -1 * sqrMagnitude;

			
			_turnSpeed += ((speed) * VelocityMultiplier) * Time.deltaTime;
			_turnSpeed *= RotationDampener;
			

			transform.Rotate(new Vector3(_turnSpeed*Axis.x,_turnSpeed*Axis.y,_turnSpeed*Axis.z),RotationSpace);
			
		}
	}
}
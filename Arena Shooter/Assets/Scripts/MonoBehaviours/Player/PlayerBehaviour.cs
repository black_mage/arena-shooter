﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Abstract;
using Assets.Scripts.Abstract.General;
using Assets.Scripts.Classes.Player;
using Assets.Scripts.Classes.Projectile.Mutators;
using Assets.Scripts.Config.Player;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.General.Movables;
using Assets.Scripts.MonoBehaviours.Managers;
using Assets.Scripts.MonoBehaviours.Weapons;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Player
{
	[RequireComponent(typeof(Movable))]
	public class PlayerBehaviour : MonoBehaviour,IMovableProvider
	{

		private PlayerController _playerController;
		private WeaponsController _weaponsController;
		private ActiveMutatorsManager _activeMutatorsManager;
		private InGameStateManager _gameStateManager;
		public GameConfiguration Config;


		public Movable Movable { get; private set; }

		public event Action<PlayerBehaviour> OnPlayerDeath;

		public BasicWeapon CurrentWeapon { get { return _weaponsController.Weapon; } }

		public IEnumerable<WeaponMutator> GetActiveMutators()
		{
			return _activeMutatorsManager.GetActiveMutators();
		}

		public void AddMutator(WeaponMutator mutator, float duration)
		{
			_activeMutatorsManager.AddMutator(mutator, duration);
		}

		public void Awake()
		{
			
			

			Movable = GetComponent<Movable>();
			_gameStateManager = FindObjectOfType<InGameStateManager>();
			_playerController = new PlayerController(this);
			_weaponsController = new AxisWeaponsController(_gameStateManager.CurrentWeapon);
			_activeMutatorsManager = new ActiveMutatorsManager();
		}

		public void Start()
		{
			_playerController.Start();
		}
		public void Update()
		{
			if (_gameStateManager.CurrentState == GameState.Playing)
			{
				
				_playerController.Update();
				_weaponsController.HandleWeapons();

				_activeMutatorsManager.Update();
				
			}
		}

		public void OnTriggerEnter(Collider other)
		{
			if (OnPlayerDeath != null)
				OnPlayerDeath(this);
			Destroy(this.gameObject);

		}

		public void ChangeWeapon(BasicWeapon weapon)
		{
			_weaponsController.ChangeWeapon(weapon);
		}
	}
}
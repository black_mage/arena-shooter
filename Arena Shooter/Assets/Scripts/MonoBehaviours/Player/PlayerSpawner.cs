﻿using System;
using Assets.Scripts.MonoBehaviours.Cameras;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;
using UnityEngine.UI;
namespace Assets.Scripts.MonoBehaviours.Player
{
	public class PlayerSpawner : MonoBehaviour
	{
		

		public PlayerBehaviour PlayerPrefab;
		public FollowCamera CameraPrefab;
		public PlayerSpawningMarker PlayerSpawningMarkerPrefab;

		public CanvasGroup SpawnDialog;

		public bool IsSpawning;


		private LevelManager _levelManager;

		private PlayerSpawningMarker _spawnMarker;
		private FollowCamera _followCamera;

		private bool _isFirstSpawn = true;
		public event Action<PlayerBehaviour> OnPlayerSpawned;
		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			IsSpawning = false;
			SpawnDialog.alpha = 0;
		}


		public void Update()
		{
			if (Input.GetButtonDown("Start") && IsSpawning || _isFirstSpawn)
			{
				_isFirstSpawn = false;
				SpawnPlayer();
			}
		}

		public void BeginSpawn()
		{
			SpawnDialog.alpha = 1;
			if (IsSpawning)
				return;

			IsSpawning = true;
			var startPosition = new Vector3(_levelManager.LevelInfo.RectBounds.CentreX, 16,
			                                _levelManager.LevelInfo.RectBounds.CentreY);

			
			_spawnMarker = (PlayerSpawningMarker)Instantiate(PlayerSpawningMarkerPrefab,startPosition,Quaternion.identity );

			if(_followCamera == null)
				_followCamera = (FollowCamera)Instantiate(CameraPrefab, new Vector3(startPosition.x,CameraPrefab.HoverHeight,startPosition.z), Quaternion.Euler(90, 0, 0));
			_followCamera.ToFollow = _spawnMarker.gameObject;
			Camera.SetupCurrent(_followCamera.GetComponent<Camera>());


		}

		

		public void SpawnPlayer()
		{

			SpawnDialog.alpha = 0;

			var player = (PlayerBehaviour)Instantiate(PlayerPrefab, new Vector3(_spawnMarker.transform.position.x, _spawnMarker.transform.position.y, _spawnMarker.transform.position.z), Quaternion.identity);
			_followCamera.ToFollow = player.gameObject;

			
			Destroy(_spawnMarker.gameObject);

			IsSpawning = false;

			if(OnPlayerSpawned != null)
				OnPlayerSpawned(player);
		}
	}
}
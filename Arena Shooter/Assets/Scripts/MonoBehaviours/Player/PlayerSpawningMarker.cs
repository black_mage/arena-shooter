﻿using Assets.Scripts.Abstract.General;
using Assets.Scripts.Classes.Level;
using Assets.Scripts.Classes.Player;
using Assets.Scripts.Data.Collision;
using Assets.Scripts.MonoBehaviours.General.Movables;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Player
{
	[RequireComponent(typeof(Movable))]
	public class PlayerSpawningMarker : MonoBehaviour,IMovableProvider
	{
		

		private RectBounds _movementBounds;
		private LevelManager _levelManager;
		private LevelInfo _levelInfo;

		private PlayerController _controller;


		public Movable Movable { get; private set; }

		public void Awake()
		{
			_controller = new PlayerController(this);
		}
		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			_levelInfo = _levelManager.LevelInfo;
			_movementBounds = _levelInfo.RectBounds;
			transform.position = new Vector3(_movementBounds.CentreX, 16, _movementBounds.CentreY);
			transform.rotation = Quaternion.Euler(90, 0, 0);

			Movable = GetComponent<Movable>();
			
			_controller.Start();
		}

		public void Update()
		{
			//_controller.Update();
			return;

			
		}
	}
}
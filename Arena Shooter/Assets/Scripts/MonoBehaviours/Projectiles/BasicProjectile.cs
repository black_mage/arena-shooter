﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code;
using Assets.Scripts.Abstract.Projectiles;
using Assets.Scripts.Classes.Collision;
using Assets.Scripts.Classes.Projectile.Modules;
using Assets.Scripts.Config.Projectiles;
using Assets.Scripts.Data.Collision;
using Assets.Scripts.Data.Enemies;
using Assets.Scripts.Data.Game;
using Assets.Scripts.MonoBehaviours.Effects;
using Assets.Scripts.MonoBehaviours.General.Destroyables;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MonoBehaviours.Projectiles
{
	public class BasicProjectile : MonoBehaviour
	{
		public ProjectileConfiguration Config;


		public BasicParticleEffect TrailEffect;




		private LevelManager _levelManager;
		protected Vector3 _velocity;

		public Vector3 Velocity { get { return _velocity; } }

		public ProjectileModuleConfiguration ModulesConfiguration;
		private List<ModuleEntry<IProjectileModule>> _modules;
		private int _burstIndex;

		public int BurstIndex { get { return _burstIndex; } }
		public Vector3 Heading { get; set; }
		public Vector3 HeadingOfMainProjectile { get; set; }


		private ModuleEntry<IProjectileModule> _followHeadingOfMainModule;
		private InGameStateManager _inGameStateManager;

		public bool IsAligningWithMainProjectile
		{
			get { return _followHeadingOfMainModule != null && ((FollowHeadingOfMain)_followHeadingOfMainModule.Module).Active; }
		}

		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			_inGameStateManager = FindObjectOfType<InGameStateManager>();




		}

		public virtual void Init(Vector3 heading, Vector3 headingOfMainProjectile, int burstIndex)
		{
			_burstIndex = burstIndex;
			_modules = new List<ModuleEntry<IProjectileModule>>();
			Heading = heading.normalized;
			HeadingOfMainProjectile = headingOfMainProjectile;
			_velocity = heading.normalized;


			AddModulesFromConfig(ModulesConfiguration);


			TrailEffect.ParticleSystem.Play();

		}

		public void AddModulesFromConfig(ProjectileModuleConfiguration config)
		{
			if (config.HomeOnEnemy.Enabled)
			{
				RemoveDuplicateFromList(typeof(HomeOnEnemy));

				_modules.Add(new ModuleEntry<IProjectileModule>(new HomeOnEnemy(this, config.HomeOnEnemy), config.HomeOnEnemy.Influence));
			}
			if (config.FollowHeadingOfMain.Enabled)
			{
				RemoveDuplicateFromList(typeof(FollowHeadingOfMain));
				_followHeadingOfMainModule = new ModuleEntry<IProjectileModule>(new FollowHeadingOfMain(this, config.FollowHeadingOfMain), config.FollowHeadingOfMain.Influence);
			}
			if (config.ZigZag.Enabled)
			{
				RemoveDuplicateFromList(typeof(ZigZag));
				_modules.Add(new ModuleEntry<IProjectileModule>(new ZigZag(this, config.ZigZag), config.ZigZag.Influence));
			}
		}

		private void RemoveDuplicateFromList(Type moduleType)
		{
			var moduleInList = _modules.Select(e => e.Module).SingleOrDefault(m => m.GetType() == moduleType);
			if (moduleInList != null)
				_modules.Remove(_modules.Single(e => e.Module == moduleInList));
		}

		public virtual void Update()
		{
			if (_inGameStateManager.CurrentState != GameState.Playing)
				return;



			var direction = _velocity.normalized;
			if (_modules.Count > 0)
			{
				direction += _modules.Select(m => m.Module.Process() * m.Influence).Aggregate((a, b) => a + b);
			}

			if (_followHeadingOfMainModule != null)
				direction += _followHeadingOfMainModule.Module.Process() * _followHeadingOfMainModule.Influence;

			_velocity = direction.normalized * Config.BaseSpeed;


			var oldPosition = transform.position;
			transform.position += _velocity * Time.deltaTime;



			var collisionInfo = BoundsCollisionChecker.CheckCollision(new CollisionCheckRequest(transform.position, oldPosition, 15,
																			_levelManager.LevelInfo.RectBounds));

			if (collisionInfo.CollisionFound)
			{
				if (Config.Bounce && Random.Range(0f, 1f) < Config.BounceChance)
				{
					Config.BounceChance *= 0.50f;
					transform.position = oldPosition;
					_velocity = collisionInfo.LineOrientation == CollisionLineOrientation.Vertical ? new Vector3(_velocity.x * -1, 0, _velocity.z) : new Vector3(_velocity.x, 0, _velocity.z * -1);

				}
				else
				{
					if (TrailEffect != null)
					{
						TrailEffect.transform.parent = null;
						TrailEffect.StopEmittingAndKill();
					}
					Destroy(this.gameObject);
				}
			}
		}

		public void OnTriggerEnter(Collider other)
		{
			var destroyable = other.gameObject.FindComponent<Destroyable>();

			if (destroyable == null)
				return;


			destroyable.TakeProjectileDamage(this);
			Destroy(this.gameObject);
		}

		public void MutateConfig(ProjectileConfiguration newConfig)
		{

			Config.BaseSpeed = Mathf.Max(newConfig.BaseSpeed, Config.BaseSpeed);
			Config.Bounce = Config.Bounce == true || newConfig.Bounce;
			Config.BounceChance = Mathf.Max(newConfig.BounceChance, Config.BounceChance);
			Config.Damage = Mathf.Max(newConfig.Damage, Config.Damage);
			Config.TurnSpeed = Mathf.Max(newConfig.TurnSpeed, Config.TurnSpeed);


		}
	}
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.UI
{
	public class LivesCounter : MonoBehaviour
	{
		[SerializeField] 
		private Text _livesField;


		public void SetLivesDisplay(int lives)
		{
			_livesField.text = lives.ToString();
		}
	}
}
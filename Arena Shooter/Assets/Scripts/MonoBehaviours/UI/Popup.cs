﻿using System;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.UI
{
	[RequireComponent(typeof(Text))]
	[RequireComponent(typeof(Animator))]
	
	public class Popup : MonoBehaviour
	{

		private Gradient _colorRange;

		[SerializeField] private float _colorInterpolation=0;

		private LevelManager _levelManager;
		private Text _textField;
		private Animator _animator;



		private PopupManager _popupManager;

		

		public void Awake()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			_popupManager = FindObjectOfType<PopupManager>();
			_colorRange = _popupManager.GetRandomGradient();
			_textField = GetComponent<Text>();


			var color = _colorRange.Evaluate(_colorInterpolation);
			var alpha = _popupManager.AlphaGradient.Evaluate(_colorInterpolation).a;


			_textField.color = new Color(color.r,color.g,color.b,alpha);


		}

		public void Init(Vector3 pos)
		{
			var rt = GetComponent<RectTransform>();
			rt.anchoredPosition3D = new Vector3(pos.x, 3072 - pos.z,250);
			rt.Rotate(-90, 0, 0);
			
		}


		public void SetText(string text)
		{
			_textField.text = text;
		}

		public void Update()
		{
			_textField.color=_colorRange.Evaluate(_colorInterpolation);

			if (Math.Abs(_colorInterpolation - 1) < Mathf.Epsilon)
				Destroy(this.gameObject);
		}
	}
}
﻿using System;
using Assets.Scripts.MonoBehaviours.Managers;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MonoBehaviours.UI
{
	public class PopupManager : MonoBehaviour
	{
		public Gradient AlphaGradient;
		public Gradient[] PopupGradients;

		[SerializeField]
		private Popup _popupPrefab;


		private LevelManager _levelManager;


		public void Start()
		{
			_levelManager = FindObjectOfType<LevelManager>();
			var rt = GetComponent<RectTransform>();

		//	transform.position = new Vector3(_levelManager.Config.Dimensions.x/2,50,_levelManager.Config.Dimensions.y/2);

		}

		public Gradient GetRandomGradient()
		{
			if(PopupGradients.Length==0)
				throw new InvalidOperationException("No popup gradients found.");
			return PopupGradients[Random.Range(0, PopupGradients.Length - 1)];
			
			
		}

		


		public void Popup(Vector3 Position,string text)
		{
			var popup = (Popup)Instantiate(_popupPrefab);
			
			popup.SetText(text);

			popup.transform.parent = this.gameObject.transform;



			popup.Init(Position);
			
			
			





		}

	}
}
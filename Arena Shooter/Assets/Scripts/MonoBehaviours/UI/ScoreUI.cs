﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.UI
{
	public class ScoreUI : MonoBehaviour
	{
		[SerializeField]
		private Text _scoreField;
		[SerializeField]
		private Text _scoreLabel;
		[SerializeField]
		private bool _showMultiplier=true;

		public void SetScore(ulong score)
		{
			_scoreField.text = score.ToString();
		}

		public void SetScore(ulong score,ulong multiplier)
		{
			_scoreLabel.text = "Score (Multiplier)";
			_scoreField.text = string.Format("{0}({1}x)", score, multiplier);
		}
	}
}
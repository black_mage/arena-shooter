﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.UI
{
	public class TimeCounter : MonoBehaviour
	{
		[SerializeField]
		private Text _displayField;


		public void SetTime(float seconds)
		{
			var span = TimeSpan.FromSeconds(seconds);
			_displayField.text = String.Format("{0:00}:{1:00}:{2:00}",span.Minutes,span.Seconds,span.Milliseconds/10);
		}
	}
}
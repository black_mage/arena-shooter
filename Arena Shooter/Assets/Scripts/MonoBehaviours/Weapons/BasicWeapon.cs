﻿using System.Collections.Generic;
using Assets.Scripts.Classes.Projectile.Mutators;
using Assets.Scripts.Data.Pickups;
using Assets.Scripts.MonoBehaviours.Projectiles;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Weapons
{
	public class BasicWeapon : MonoBehaviour
	{

		public BasicProjectile Projectile;
		
		public float FireRate= 0.5f;

		public int NumberOfBarrels = 1;

		public float AngleBetweenBarrels = 0f;


		private bool _hasFired;
		private float _coolDown;
		private float _lastAngle;

		public bool CanFire { get { return _coolDown <= 0f; } }

		

		public void Start()
		{
			_hasFired = false;
			
		}

		public void Update()
		{
			
			if (_coolDown >= 0f)
				_coolDown -= Time.deltaTime;

	


		}
		


		public void Fire(float angle,IEnumerable<WeaponMutator> mutators )
		{
			if(_coolDown >0f)
			{
				return;
			}

			var mainProjectileHeading = new Vector3(Mathf.Sin(angle*Mathf.Deg2Rad), 0, Mathf.Cos(angle*Mathf.Deg2Rad));
			var currentAngle = NumberOfBarrels > 1 ? angle-((AngleBetweenBarrels*(NumberOfBarrels-1))/2) : angle;

			var burstIndex = -1*(NumberOfBarrels/2);

			

			for (var p = 0; p < NumberOfBarrels; p++)
			{
				var projectile = (BasicProjectile) Instantiate(Projectile, transform.position, Quaternion.Euler(0, currentAngle, 0));
				projectile.Init(new Vector3(Mathf.Sin(currentAngle*Mathf.Deg2Rad), 0, Mathf.Cos(currentAngle*Mathf.Deg2Rad)),mainProjectileHeading,burstIndex);
				foreach(var mutator in mutators)
					mutator.Mutate(projectile);
				currentAngle += AngleBetweenBarrels;

				burstIndex++;
				if (burstIndex == 0 && NumberOfBarrels % 2 == 0)
					burstIndex++;
			}
			_coolDown = FireRate;
			_hasFired = true;
			_lastAngle = angle;
		}

		public Vector2 GetCurrentFiringAngles()
		{
			if(!_hasFired)
				return Vector2.zero;
			var angleRange = AngleBetweenBarrels*(NumberOfBarrels - 1);
			return new Vector2(_lastAngle-(angleRange/2f),_lastAngle+(angleRange/2f));
		}
	}
}
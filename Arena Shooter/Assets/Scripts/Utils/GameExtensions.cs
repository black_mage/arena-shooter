﻿using UnityEngine;

namespace Assets.Code
{
	public static class GameExtensions
	{
		 public static T FindComponent<T>(this GameObject that) where T: Component
		 {
			 var component = that.GetComponent<T>();
			 if(component != null)
			 {
				 return component;
			 }

			 var proxy = that.GetComponent<ComponentProxy>();

			 if (proxy == null)
				 return null;

			 if(proxy.ProxyFor == null)
			 {
				 Debug.LogWarning("ComponentProxy didn't specify what it was a proxy for.",that);
				 return null;
			 }

			 return proxy.ProxyFor.GetComponent<T>();
		 }
	}
}
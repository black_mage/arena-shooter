﻿Shader "Warp"
{
	Properties
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Warp1_Position ("Warp 1 Position", Vector) = (0, 0, 0)
		_Warp1_Radius ("Warp 1 Radius", float) = 0
		_Warp1_Strength ("Warp 1 Strength", float) = 0
		 _Color ("Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags
		{
			"Queue"           = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType"      = "Transparent"
		}
		
		Blend    SrcAlpha OneMinusSrcAlpha
		ZWrite   Off
		Cull     Off
		
		CGPROGRAM
			#pragma surface Surf Lambert vertex:Vert alpha:blend
			#pragma multi_compile WARP_0 WARP_1 WARP_2

			sampler2D _MainTex;
			float4    _Warp1_Position;
			float4	  _Color;
			float     _Warp1_Radius;
			float     _Warp1_Strength;
			
			struct Input
			{
				float2 uv_MainTex;
			};
			
			void Vert(inout appdata_full v)
			{
				float4 mPos = mul(_Object2World, v.vertex);
				float3 warpVec;
				float  warpDst, warpStr; 
				
				// Warp 1
				warpVec = (mPos.xyz - _Warp1_Position); // Vector between warp and vertex
				warpDst = saturate(length(warpVec) / _Warp1_Radius); // Find 0..1 distance
				warpStr = 1.0f - warpDst; // Invert
				
				mPos.xyz += warpVec * warpStr * _Warp1_Strength; // Pull or push vertex to the point
				
				v.vertex = mul(_World2Object, mPos);
			}
			
			void Surf(Input i, inout SurfaceOutput o)
			{
				float4 mainTex = tex2D(_MainTex, i.uv_MainTex) * _Color;
				
				
				o.Albedo = mainTex.rgb;
				o.Alpha  = mainTex.a;
			}
		ENDCG
	} // SubShader
} // Shader